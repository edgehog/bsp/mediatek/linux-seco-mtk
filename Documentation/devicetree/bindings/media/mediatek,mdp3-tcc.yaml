# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/media/mediatek,mdp3-tcc.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: MediaTek Media Data Path 3 TCC Device Tree Bindings

maintainers:
  - Matthias Brugger <matthias.bgg@gmail.com>

description: |
  One of Media Data Path 3 (MDP3) components used to support
  HDR gamma curve conversion HDR displays.

properties:
  compatible:
    enum:
      - mediatek,mt8195-mdp3-tcc

  reg:
    maxItems: 1

  mediatek,gce-client-reg:
    $ref: /schemas/types.yaml#/definitions/phandle-array
    items:
      items:
        - description: phandle of GCE
        - description: GCE subsys id
        - description: register offset
        - description: register size
    description: The register of client driver can be configured by gce with
      4 arguments defined in this property. Each GCE subsys id is mapping to
      a client defined in the header include/dt-bindings/gce/<chip>-gce.h.

  clocks:
    minItems: 1

  power-domains:
    maxItems: 1

required:
  - compatible
  - reg
  - mediatek,gce-client-reg
  - clocks
  - power-domains

additionalProperties: false

examples:
  - |
    #include <dt-bindings/clock/mt8195-clk.h>
    #include <dt-bindings/gce/mt8195-gce.h>
    #include <dt-bindings/power/mt8195-power.h>

    mdp3-tcc0@1400b000 {
      compatible = "mediatek,mt8195-mdp3-tcc";
      reg = <0x1400b000 0x1000>;
      mediatek,gce-client-reg = <&gce1 SUBSYS_1400XXXX 0xb000 0x1000>;
      clocks = <&vppsys0 CLK_VPP0_MDP_TCC>;
      power-domains = <&spm MT8195_POWER_DOMAIN_VPPSYS0>;
    };
