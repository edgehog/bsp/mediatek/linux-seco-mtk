# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/sound/mt8188-mt6359.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: MediaTek MT8188 ASoC sound card

maintainers:
  - Trevor Wu <trevor.wu@mediatek.com>

properties:
  compatible:
    const: mediatek,mt8188-mt6359-evb

  model:
    $ref: /schemas/types.yaml#/definitions/string
    description: User specified audio sound card name

  audio-routing:
    $ref: /schemas/types.yaml#/definitions/non-unique-string-array
    description:
      A list of the connections between audio components. Each entry is a
      sink/source pair of strings. Valid names could be the input or output
      widgets of audio components, power supplies, MicBias of codec and the
      software switch.

  mediatek,platform:
    $ref: /schemas/types.yaml#/definitions/phandle
    description: The phandle of MT8188 ASoC platform.

  mediatek,dptx-codec:
    $ref: /schemas/types.yaml#/definitions/phandle
    description: The phandle of MT8188 Display Port Tx codec node.

  mediatek,hdmi-codec:
    $ref: /schemas/types.yaml#/definitions/phandle
    description: The phandle of MT8188 HDMI codec node.

additionalProperties: false

required:
  - compatible
  - mediatek,platform

examples:
  - |

    sound: mt8188-sound {
        compatible = "mediatek,mt8188-mt6359-evb";
        mediatek,platform = <&afe>;
        pinctrl-names = "default";
        pinctrl-0 = <&aud_pins_default>;
        audio-routing =
            "Headphone", "Headphone L",
            "Headphone", "Headphone R",
            "AIN1", "Headset Mic";
    };

...
