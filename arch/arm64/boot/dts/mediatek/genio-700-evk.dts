// SPDX-License-Identifier: (GPL-2.0 OR MIT)
/*
 * Copyright (C) 2022 MediaTek Inc.
 * Author: Chris Chen <chris-qj.chen@mediatek.com>
 */
/dts-v1/;
#include "mt8188.dtsi"
#include "mt6359.dtsi"
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/interrupt-controller/irq.h>
#include <dt-bindings/pinctrl/mediatek,mt8188-pinfunc.h>
#include <dt-bindings/usb/pd.h>

/ {
	model = "MediaTek Genio-700 EVK";
	compatible = "mediatek,mt8390-evk", "mediatek,mt8188";

	aliases {
		serial0 = &uart0;
		dsi0 = &dsi0;
		ethernet0 = &eth;
	};

	backlight_lcd0: backlight-lcd0 {
		compatible = "pwm-backlight";
		pwms = <&disp_pwm0 0 500000>;
		enable-gpios = <&pio 8 GPIO_ACTIVE_HIGH>;
		brightness-levels = <0 1023>;
		num-interpolated-steps = <1023>;
		default-brightness-level = <576>;
		status = "disabled";
	};

	chosen {
		stdout-path = "serial0:921600n8";
	};

	edp_panel_fixed_3v3: regulator@0 {
		compatible = "regulator-fixed";
		regulator-name = "edp_panel_3v3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		enable-active-high;
		gpio = <&pio 15 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&edp_panel_3v3_en_pins>;
	};

	memory@40000000 {
		device_type = "memory";
		reg = <0 0x40000000 0x2 0x00000000>;
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		adsp_dma_mem_reserved: adsp-dma-mem-region {
			compatible = "shared-dma-pool";
			reg = <0 0x61000000 0 0x100000>;
			no-map;
		};

		adsp_mem_reserved: adsp-mem-region {
			compatible = "shared-dma-pool";
			no-map;
			reg = <0 0x60000000 0 0xf00000>;
		};

		apu_reserve_memory: apu-reserve-memory{
			compatible = "shared-dma-pool";
			size = <0 0x1400000>; //20 MB
			alignment = <0 0x10000>;
			reg = <0 0x55000000 0 0x1400000>;
		};

		afe_dma_mem_reserved: snd-dma-mem-region {
			compatible = "shared-dma-pool";
			no-map;
			reg = <0 0x60f00000 0 0x100000>;
		};

		vpu_reserve_memory: vpu-reserve-memory {
			compatible = "shared-dma-pool";
			size = <0 0x1400000>; //20 MB
			alignment = <0 0x10000>;
			reg = <0 0x57000000 0 0x1400000>;
		};

		/* 12 MiB reserved for OP-TEE (BL32)
		 * +-----------------------+ 0x43e0_0000
		 * |      SHMEM 2MiB       |
		 * +-----------------------+ 0x43c0_0000
		 * |        | TA_RAM  8MiB |
		 * + TZDRAM +--------------+ 0x4340_0000
		 * |        | TEE_RAM 2MiB |
		 * +-----------------------+ 0x4320_0000
		 */
		optee_reserved: optee@43200000 {
			no-map;
			reg = <0 0x43200000 0 0x00c00000>;
		};

		scp_mem_reserved: scp-mem-region {
			compatible = "shared-dma-pool";
			reg = <0 0x50000000 0 0x2900000>;
			no-map;
		};

		/* 2 MiB reserved for ARM Trusted Firmware (BL31) */
		bl31_secmon_reserved: secmon@54600000 {
			no-map;
			reg = <0 0x54600000 0x0 0x200000>;
		};
	};

	firmware {
		optee {
			compatible = "linaro,optee-tz";
			method = "smc";
		};
	};

	usb_p0_vbus: usb0-otg-vbus {
		compatible = "regulator-fixed";
		regulator-name = "p0_vbus";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&pio 84 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	usb_p1_vbus: usb-p1-vbus {
		compatible = "regulator-fixed";
		regulator-name = "p1_vbus";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&pio 87 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	usb_p2_vbus: usb-p2-vbus {
		compatible = "regulator-fixed";
		regulator-name = "p2_vbus";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		enable-active-high;
	};

	hub3v3: regulator@4 {
		compatible = "regulator-fixed";
		regulator-name = "vhub3v3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = <&pio 112 0>; /* HUB_3V3_EN */
		startup-delay-us = <10000>;
		enable-active-high;
	};

	onboard_hub: regulator@5 {
		compatible = "regulator-fixed";
		regulator-name = "hub_reset";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		gpio = <&pio 7 0>; /* HUB_RESET */
		vin-supply = <&hub3v3>;
		enable-active-low;
	};

	lcm1_tp_avdd: lcm1-tp-avdd {
		compatible = "regulator-fixed";
		regulator-name = "lcm1-tp-avdd";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = <&pio 119 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	VDD_5V: VDD-5V {
		compatible = "regulator-fixed";
		regulator-name = "VDD-5V";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&pio 10 GPIO_ACTIVE_HIGH>;
		enable-active-high;
		regulator-always-on;
	};

	VDD_3V3: VDD-3V3 {
		compatible = "regulator-fixed";
		regulator-name = "VDD-3V3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = <&pio 9 GPIO_ACTIVE_HIGH>;
		enable-active-high;
		regulator-always-on;
	};

	sdio_fixed_3v3: regulator@2 {
		compatible = "regulator-fixed";
		regulator-name = "sdio_card";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = <&pio 74 0>;
		enable-active-high;
		regulator-always-on;
	};

	sdio_fixed_1v8: regulator@3 {
		compatible = "regulator-fixed";
		regulator-name = "sdio_io";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		enable-active-high;
		regulator-always-on;
	};

	amic_codec: amic-codec {
		#sound-dai-cells = <0>;
		compatible = "dmic-codec";
		num-channels = <2>;
		wakeup-delay-ms = <200>;
	};

	dmic_codec: dmic-codec {
		#sound-dai-cells = <0>;
		compatible = "dmic-codec";
		num-channels = <2>;
		wakeup-delay-ms = <30>;
	};

	pmic_temp: pmic-temp {
		compatible = "mediatek,mt6359-pmic-temp";
		io-channels =
			<&pmic_auxadc AUXADC_CHIP_TEMP>,
			<&pmic_auxadc AUXADC_VCORE_TEMP>,
			<&pmic_auxadc AUXADC_VPROC_TEMP>,
			<&pmic_auxadc AUXADC_VGPU_TEMP>;
		io-channel-names =
			"pmic_chip_temp",
			"pmic_buck1_temp",
			"pmic_buck2_temp",
			"pmic_buck3_temp";

		#thermal-sensor-cells = <1>;
		nvmem-cells = <&thermal_efuse_data1>;
		nvmem-cell-names = "e_data1";
	};

	tboard_thermistor1: tboard-thermistor1 {
		compatible = "generic-adc-thermal";
		#thermal-sensor-cells = <0>;
		io-channels = <&auxadc 0>;
		io-channel-names = "sensor-channel";
		temperature-lookup-table = <    (-5000) 985
						0 860
						5000 740
						10000 629
						15000 530
						20000 442
						25000 367
						30000 304
						35000 251
						40000 207
						45000 171
						50000 141
						55000 117
						60000 97
						65000 81
						70000 67
						75000 56
						80000 47
						85000 40
						90000 34
						95000 29
						100000 25
						105000 21
						110000 18
						115000 15
						120000 13
						125000 12>;
	};
};

&thermal_zones {
	ap-ntc {
		polling-delay = <1000>; /* milliseconds */
		polling-delay-passive = <0>; /* milliseconds */
		thermal-sensors = <&tboard_thermistor1>;
	};
	pmic-temp {
		polling-delay = <1000>; /* milliseconds */
		polling-delay-passive = <1000>; /* milliseconds */
		thermal-sensors = <&pmic_temp 0>;

		trips {
			pmic_temp_crit: pmic-temp-crit@0 {
				temperature = <130000>;
				hysteresis = <2000>;
				type = "critical";
			};
		};
	};
	pmic-vcore {
		polling-delay = <0>; /* milliseconds */
		polling-delay-passive = <0>; /* milliseconds */
		thermal-sensors = <&pmic_temp 1>;
	};
	pmic-vproc {
		polling-delay = <0>; /* milliseconds */
		polling-delay-passive = <0>; /* milliseconds */
		thermal-sensors = <&pmic_temp 2>;
	};
	pmic-vgpu {
		polling-delay = <0>; /* milliseconds */
		polling-delay-passive = <0>; /* milliseconds */
		thermal-sensors = <&pmic_temp 3>;
	};
};

&auxadc {
	status = "okay";
};

&afe {
	memory-region = <&afe_dma_mem_reserved>;
	#sound-dai-cells = <0>;
	mediatek,dmic-iir-on;
	mediatek,dmic-clk-mono;
	status = "okay";
};

&adsp {
	memory-region = <&adsp_dma_mem_reserved>,
		<&adsp_mem_reserved>;
};

&eth {
	phy-mode ="rgmii-rxid";
	phy-handle = <&ethernet_phy0>;
	snps,reset-gpio = <&pio 147 GPIO_ACTIVE_HIGH>;
	snps,reset-delay-us = <0 10000 10000>;
	mediatek,tx-delay-ps = <2030>;
	mediatek,mac-wol;
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&eth_default_pins>;
	pinctrl-1 = <&eth_sleep_pins>;
	status = "okay";

	mdio {
		compatible = "snps,dwmac-mdio";
		#address-cells = <1>;
		#size-cells = <0>;
		ethernet_phy0: ethernet-phy@1 {
			compatible = "ethernet-phy-id001c.c916";
			reg = <0x1>;
		};
	};
};

&dpi1 {
	status = "okay";
};

&dp_intf1 {
	status = "disabled";
	mediatek,oob-hpd;
	ports {
		port {
			dp_intf1_out: endpoint {
				remote-endpoint = <&dptx_in>;
			};
		};
	};
};

&dp_tx {
	pinctrl-names = "default";
	pinctrl-0 = <&dptx_pin>;
	status = "disabled";

	ports {
		#address-cells = <1>;
		#size-cells = <0>;
		port {
			dptx_in: endpoint {
				remote-endpoint = <&dp_intf1_out>;
			};
		};
	};
};

&dsi0 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	poweron-in-hs-mode = <0>;

	panel@0 {
		compatible = "startek,kd070fhfid015";
		status = "okay";
		reg = <0>;
		pinctrl-names = "default";
		pinctrl-0 = <&panel_pins_default>;
		reset-gpios = <&pio 25 GPIO_ACTIVE_HIGH>;
		dcdc-gpios = <&pio 45 GPIO_ACTIVE_HIGH>;
		enable-gpios = <&pio 111 GPIO_ACTIVE_HIGH>;
		port {
			panel0_in: endpoint {
				remote-endpoint = <&dsi0_out>;
			};
		};
	};

	ports {
		port {
			dsi0_out: endpoint {
				remote-endpoint = <&panel0_in>;
			};
		};
	};
};

&hdmi0 {
	status = "okay";
};

&pio {
	aud_pins_default: audiodefault {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO101__FUNC_O_AUD_CLK_MOSI>,
				 <PINMUX_GPIO102__FUNC_O_AUD_SYNC_MOSI>,
				 <PINMUX_GPIO103__FUNC_O_AUD_DAT_MOSI0>,
				 <PINMUX_GPIO104__FUNC_O_AUD_DAT_MOSI1>,
				 <PINMUX_GPIO105__FUNC_I0_AUD_DAT_MISO0>,
				 <PINMUX_GPIO106__FUNC_I0_AUD_DAT_MISO1>,
				 <PINMUX_GPIO107__FUNC_B0_I2SIN_MCK>,
				 <PINMUX_GPIO108__FUNC_B0_I2SIN_BCK>,
				 <PINMUX_GPIO109__FUNC_B0_I2SIN_WS>,
				 <PINMUX_GPIO110__FUNC_I0_I2SIN_D0>,
				 <PINMUX_GPIO114__FUNC_O_I2SO2_MCK>,
				 <PINMUX_GPIO115__FUNC_B0_I2SO2_BCK>,
				 <PINMUX_GPIO116__FUNC_B0_I2SO2_WS>,
				 <PINMUX_GPIO117__FUNC_O_I2SO2_D0>,
				 <PINMUX_GPIO118__FUNC_O_I2SO2_D1>,
				 <PINMUX_GPIO125__FUNC_O_DMIC1_CLK>,
				 <PINMUX_GPIO126__FUNC_I0_DMIC1_DAT>,
				 <PINMUX_GPIO128__FUNC_O_DMIC2_CLK>,
				 <PINMUX_GPIO129__FUNC_I0_DMIC2_DAT>,
				 <PINMUX_GPIO121__FUNC_B0_PCM_CLK>,
				 <PINMUX_GPIO122__FUNC_B0_PCM_SYNC>,
				 <PINMUX_GPIO124__FUNC_I0_PCM_DI>;
		};
	};

	edp_panel_3v3_en_pins: edp-panel-3v3-en-pins {
		pins1 {
			pinmux = <PINMUX_GPIO15__FUNC_B_GPIO15>;
			output-high;
		};
	};

	dptx_pin: dptx-pin {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO46__FUNC_I0_DP_TX_HPD>;
			bias-pull-up;
		};
	};

	eth_default_pins: eth-default-pins {
		pins-txd {
			pinmux = <PINMUX_GPIO131__FUNC_O_GBE_TXD3>,
				 <PINMUX_GPIO132__FUNC_O_GBE_TXD2>,
				 <PINMUX_GPIO133__FUNC_O_GBE_TXD1>,
				 <PINMUX_GPIO134__FUNC_O_GBE_TXD0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
		pins-cc {
			pinmux = <PINMUX_GPIO139__FUNC_B0_GBE_TXC>,
				 <PINMUX_GPIO142__FUNC_O_GBE_TXEN>,
				 <PINMUX_GPIO141__FUNC_I0_GBE_RXDV>,
				 <PINMUX_GPIO140__FUNC_I0_GBE_RXC>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
		pins-rxd {
			pinmux = <PINMUX_GPIO135__FUNC_I0_GBE_RXD3>,
				 <PINMUX_GPIO136__FUNC_I0_GBE_RXD2>,
				 <PINMUX_GPIO137__FUNC_I0_GBE_RXD1>,
				 <PINMUX_GPIO138__FUNC_I0_GBE_RXD0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
		pins-mdio {
			pinmux = <PINMUX_GPIO143__FUNC_O_GBE_MDC>,
				 <PINMUX_GPIO144__FUNC_B1_GBE_MDIO>;
			drive-strength = <MTK_DRIVE_8mA>;
			input-enable;
		};
		pins-power {
			pinmux = <PINMUX_GPIO145__FUNC_B_GPIO145>,
				 <PINMUX_GPIO146__FUNC_B_GPIO146>;
			output-high;
		};
	};

	eth_sleep_pins: eth-sleep-pins {
		pins-txd {
			pinmux = <PINMUX_GPIO131__FUNC_B_GPIO131>,
				 <PINMUX_GPIO132__FUNC_B_GPIO132>,
				 <PINMUX_GPIO133__FUNC_B_GPIO133>,
				 <PINMUX_GPIO134__FUNC_B_GPIO134>;
		};
		pins-cc {
			pinmux = <PINMUX_GPIO139__FUNC_B_GPIO139>,
				 <PINMUX_GPIO142__FUNC_B_GPIO142>,
				 <PINMUX_GPIO141__FUNC_B_GPIO141>,
				 <PINMUX_GPIO140__FUNC_B_GPIO140>;
		};
		pins-rxd {
			pinmux = <PINMUX_GPIO135__FUNC_B_GPIO135>,
				 <PINMUX_GPIO136__FUNC_B_GPIO136>,
				 <PINMUX_GPIO137__FUNC_B_GPIO137>,
				 <PINMUX_GPIO138__FUNC_B_GPIO138>;
		};
		pins-mdio {
			pinmux = <PINMUX_GPIO143__FUNC_B_GPIO143>,
				 <PINMUX_GPIO144__FUNC_B_GPIO144>;
			input-disable;
			bias-disable;
		};
	};

	uart0_pins: uart0-pins {
		pins {
			pinmux = <PINMUX_GPIO31__FUNC_O_UTXD0>,
				 <PINMUX_GPIO32__FUNC_I1_URXD0>;
			bias-pull-up;
		};
	};

	uart1_pins: uart1-pins {
		pins {
			pinmux = <PINMUX_GPIO33__FUNC_O_UTXD1>,
				 <PINMUX_GPIO34__FUNC_I1_URXD1>;
			bias-pull-up;
		};
	};

	uart2_pins: uart2-pins {
		pins {
			pinmux = <PINMUX_GPIO35__FUNC_O_UTXD2>,
				 <PINMUX_GPIO36__FUNC_I1_URXD2>;
			bias-pull-up;
		};
	};

	i2c0_pin: i2c0-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO56__FUNC_B1_SDA0>,
				 <PINMUX_GPIO55__FUNC_B1_SCL0>;
			bias-pull-up = <MTK_PULL_SET_RSEL_011>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c1_pin: i2c1-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO58__FUNC_B1_SDA1>,
				 <PINMUX_GPIO57__FUNC_B1_SCL1>;
			bias-pull-up = <MTK_PULL_SET_RSEL_011>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c2_pin: i2c2-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO60__FUNC_B1_SDA2>,
				 <PINMUX_GPIO59__FUNC_B1_SCL2>;
			bias-pull-up = <MTK_PULL_SET_RSEL_011>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c3_pin: i2c3-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO62__FUNC_B1_SDA3>,
				 <PINMUX_GPIO61__FUNC_B1_SCL3>;
			bias-pull-up = <MTK_PULL_SET_RSEL_011>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c4_pin: i2c4-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO64__FUNC_B1_SDA4>,
				 <PINMUX_GPIO63__FUNC_B1_SCL4>;
			bias-pull-up = <MTK_PULL_SET_RSEL_011>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c5_pin: i2c5-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO66__FUNC_B1_SDA5>,
				 <PINMUX_GPIO65__FUNC_B1_SCL5>;
			bias-pull-up = <MTK_PULL_SET_RSEL_011>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	i2c6_pin: i2c6-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO68__FUNC_B1_SDA6>,
				 <PINMUX_GPIO67__FUNC_B1_SCL6>;
			bias-pull-up = <MTK_PULL_SET_RSEL_011>;
			mediatek,drive-strength-adv = <7>;
		};
	};

	rt1715_int: rt1715_int_pin {
		pins_cmd0_dat {
			pinmux = <PINMUX_GPIO12__FUNC_B_GPIO12>;
			bias-pull-up;
			input-enable;
		};
	};

	kpd_gpios_def_cfg: keypad-pins-default {
		pins_kp {
			pinmux = <PINMUX_GPIO44__FUNC_B1_KPROW0>,
					<PINMUX_GPIO43__FUNC_B1_KPCOL1>,
					<PINMUX_GPIO42__FUNC_B1_KPCOL0>;
		};
	};

	panel_pins_default: panel-pins-default {
		panel-dcdc {
			pinmux = <PINMUX_GPIO45__FUNC_B_GPIO45>;
			output-low;
		};

		panel-en {
			pinmux = <PINMUX_GPIO111__FUNC_B_GPIO111>;
			output-low;
		};

		panel-rst {
			pinmux = <PINMUX_GPIO25__FUNC_B_GPIO25>;
			output-high;
		};
	};

	mmc0_pins_default: mmc0default {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO161__FUNC_B1_MSDC0_DAT0>,
				 <PINMUX_GPIO160__FUNC_B1_MSDC0_DAT1>,
				 <PINMUX_GPIO159__FUNC_B1_MSDC0_DAT2>,
				 <PINMUX_GPIO158__FUNC_B1_MSDC0_DAT3>,
				 <PINMUX_GPIO154__FUNC_B1_MSDC0_DAT4>,
				 <PINMUX_GPIO153__FUNC_B1_MSDC0_DAT5>,
				 <PINMUX_GPIO152__FUNC_B1_MSDC0_DAT6>,
				 <PINMUX_GPIO151__FUNC_B1_MSDC0_DAT7>,
				 <PINMUX_GPIO156__FUNC_B1_MSDC0_CMD>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins-clk {
			pinmux = <PINMUX_GPIO157__FUNC_B1_MSDC0_CLK>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins-rst {
			pinmux = <PINMUX_GPIO155__FUNC_O_MSDC0_RSTB>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};

	mmc0_pins_uhs: mmc0uhs{
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO161__FUNC_B1_MSDC0_DAT0>,
				 <PINMUX_GPIO160__FUNC_B1_MSDC0_DAT1>,
				 <PINMUX_GPIO159__FUNC_B1_MSDC0_DAT2>,
				 <PINMUX_GPIO158__FUNC_B1_MSDC0_DAT3>,
				 <PINMUX_GPIO154__FUNC_B1_MSDC0_DAT4>,
				 <PINMUX_GPIO153__FUNC_B1_MSDC0_DAT5>,
				 <PINMUX_GPIO152__FUNC_B1_MSDC0_DAT6>,
				 <PINMUX_GPIO151__FUNC_B1_MSDC0_DAT7>,
				 <PINMUX_GPIO156__FUNC_B1_MSDC0_CMD>;
			input-enable;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins-clk {
			pinmux = <PINMUX_GPIO157__FUNC_B1_MSDC0_CLK>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins-ds {
			pinmux = <PINMUX_GPIO162__FUNC_B0_MSDC0_DSL>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins-rst {
			pinmux = <PINMUX_GPIO155__FUNC_O_MSDC0_RSTB>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};

	mmc1_pins_default: mmc1default {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO163__FUNC_B1_MSDC1_CMD>,
				 <PINMUX_GPIO165__FUNC_B1_MSDC1_DAT0>,
				 <PINMUX_GPIO166__FUNC_B1_MSDC1_DAT1>,
				 <PINMUX_GPIO167__FUNC_B1_MSDC1_DAT2>,
				 <PINMUX_GPIO168__FUNC_B1_MSDC1_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins-clk {
			pinmux = <PINMUX_GPIO164__FUNC_B1_MSDC1_CLK>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins-insert {
			pinmux = <PINMUX_GPIO2__FUNC_B_GPIO2>;
			bias-pull-up;
		};
	};

	mmc1_pins_uhs: mmc1uhs {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO163__FUNC_B1_MSDC1_CMD>,
				 <PINMUX_GPIO165__FUNC_B1_MSDC1_DAT0>,
				 <PINMUX_GPIO166__FUNC_B1_MSDC1_DAT1>,
				 <PINMUX_GPIO167__FUNC_B1_MSDC1_DAT2>,
				 <PINMUX_GPIO168__FUNC_B1_MSDC1_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins-clk {
			pinmux = <PINMUX_GPIO164__FUNC_B1_MSDC1_CLK>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};
	};

	mmc2_pins_default: mmc2default {
		pins-clk {
			pinmux = <PINMUX_GPIO170__FUNC_B1_MSDC2_CLK>;
			drive-strength = <MTK_DRIVE_4mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins-cmd-dat {
			pinmux = <PINMUX_GPIO169__FUNC_B1_MSDC2_CMD>,
				 <PINMUX_GPIO171__FUNC_B1_MSDC2_DAT0>,
				 <PINMUX_GPIO172__FUNC_B1_MSDC2_DAT1>,
				 <PINMUX_GPIO173__FUNC_B1_MSDC2_DAT2>,
				 <PINMUX_GPIO174__FUNC_B1_MSDC2_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins-pcm {
			pinmux = <PINMUX_GPIO123__FUNC_O_PCM_DO>;
		};
	};

	mmc2_pins_uhs: mmc2uhs {
		pins-clk {
			pinmux = <PINMUX_GPIO170__FUNC_B1_MSDC2_CLK>;
			drive-strength = <MTK_DRIVE_4mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins-cmd-dat {
			pinmux = <PINMUX_GPIO169__FUNC_B1_MSDC2_CMD>,
				 <PINMUX_GPIO171__FUNC_B1_MSDC2_DAT0>,
				 <PINMUX_GPIO172__FUNC_B1_MSDC2_DAT1>,
				 <PINMUX_GPIO173__FUNC_B1_MSDC2_DAT2>,
				 <PINMUX_GPIO174__FUNC_B1_MSDC2_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};

	mmc2_pins_eint: dat1-eint {
		pins-dat1 {
			pinmux = <PINMUX_GPIO172__FUNC_B_GPIO172>;
			input-enable;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};

	mmc2_pins_dat1: sdio-dat1 {
		pins-dat1 {
			pinmux = <PINMUX_GPIO172__FUNC_B1_MSDC2_DAT1>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};

	wifi_pins_pwrseq: wifipwrseq {
		pins-wifi-enable {
			pinmux = <PINMUX_GPIO127__FUNC_B_GPIO127>;
			output-low;
		};
	};

	spi0_pins: spi0-pins {
		pins-spi {
			pinmux = <PINMUX_GPIO69__FUNC_O_SPIM0_CSB>,
				<PINMUX_GPIO70__FUNC_O_SPIM0_CLK>,
				<PINMUX_GPIO71__FUNC_B0_SPIM0_MOSI>,
				<PINMUX_GPIO72__FUNC_B0_SPIM0_MISO>;
			bias-disable;
		};
	};

	spi1_pins: spi1-pins {
		pins-spi {
			pinmux = <PINMUX_GPIO75__FUNC_O_SPIM1_CSB>,
				<PINMUX_GPIO76__FUNC_O_SPIM1_CLK>,
				<PINMUX_GPIO77__FUNC_B0_SPIM1_MOSI>,
				<PINMUX_GPIO78__FUNC_B0_SPIM1_MISO>;
			bias-disable;
		};
	};

	spi2_pins: spi2-pins {
		pins-spi {
			pinmux = <PINMUX_GPIO79__FUNC_O_SPIM2_CSB>,
				<PINMUX_GPIO80__FUNC_O_SPIM2_CLK>,
				<PINMUX_GPIO81__FUNC_B0_SPIM2_MOSI>,
				<PINMUX_GPIO82__FUNC_B0_SPIM2_MISO>;
			bias-disable;
		};
	};

	pcie_pins_default: pcie-default {
		mux {
			pinmux = <PINMUX_GPIO47__FUNC_I1_WAKEN>,
				 <PINMUX_GPIO48__FUNC_O_PERSTN>,
				 <PINMUX_GPIO49__FUNC_B1_CLKREQN>;
			bias-pull-up;
		};
	};

	usb_default: usbdefault {
		iddig-pin {
			pinmux = <PINMUX_GPIO83__FUNC_B_GPIO83>;
			input-enable;
			bias-pull-up;
		};

		vbus-pins {
			pinmux = <PINMUX_GPIO84__FUNC_O_USB_DRVVBUS>;
			output-high;
		};

		valid-pin {
			pinmux = <PINMUX_GPIO85__FUNC_I0_VBUSVALID>;
			input-enable;
		};
	};

	usb1_default: usbp1default {
		valid_pin {
			pinmux = <PINMUX_GPIO88__FUNC_I0_VBUSVALID_1P>;
			input-enable;
		};

		usb-hub-3v3-en-pin {
			pinmux = <PINMUX_GPIO112__FUNC_B_GPIO112>;
			output-high;
		};
	};

	touch_pins: touchdefault {
		touch_pin_irq: pin-irq {
			pinmux = <PINMUX_GPIO6__FUNC_B_GPIO6>;
			input-enable;
			bias-disable;
		};

		touch_pin_reset: pin-reset {
			pinmux = <PINMUX_GPIO5__FUNC_B_GPIO5>;
			output-high;
		};
	};
};

&i2c0 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c0_pin>;
	clock-frequency = <400000>;
	status = "okay";
	touchscreen@5d {
		compatible = "goodix,gt9271";
		reg = <0x5d>;
		interrupt-parent = <&pio>;
		interrupts = <6 IRQ_TYPE_EDGE_RISING>;
		irq-gpios = <&pio 6 GPIO_ACTIVE_HIGH>;
		reset-gpios = <&pio 5 GPIO_ACTIVE_HIGH>;
		AVDD28-supply = <&lcm1_tp_avdd>;
		VDDIO-supply = <&mt6359_vio18_ldo_reg>;
		pinctrl-names = "default";
		pinctrl-0 = <&touch_pins>;
	};
};

&i2c1 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c1_pin>;
	clock-frequency = <400000>;
	status = "okay";

	it5205fn: it5205fn@48 {
		compatible = "mediatek,it5205fn";
		reg = <0x48>;
		type3v3-supply = <&mt6359_vcn33_1_bt_ldo_reg>;
		svid = /bits/ 16 <0xff01>;
		status = "okay";
	};
};

&i2c2 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c2_pin>;
	clock-frequency = <400000>;
	status = "okay";
};

&i2c3 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c3_pin>;
	clock-frequency = <400000>;
	status = "okay";
};

&i2c4 {
	pinctrl-names = "default", "default";
	pinctrl-0 = <&i2c4_pin>;
	pinctrl-1 = <&rt1715_int>;
	clock-frequency = <1000000>;
	status = "okay";

	rt1715@4e {
		compatible = "richtek,rt1715";
		reg = <0x4e>;
		interrupts-extended = <&pio 12 IRQ_TYPE_LEVEL_LOW>;
		vbus-supply = <&usb_p1_vbus>;
		status = "okay";

		usb_con: connector {
			compatible = "usb-c-connector";
			label = "USB-C";
			data-role = "dual";
			power-role = "dual";
			try-power-role = "sink";
			source-pdos = <PDO_FIXED(5000, 2000, PDO_FIXED_DUAL_ROLE |
							PDO_FIXED_DATA_SWAP)>;
			sink-pdos = <PDO_FIXED(5000, 2000, PDO_FIXED_DUAL_ROLE |
							PDO_FIXED_DATA_SWAP)>;
			op-sink-microwatt = <10000000>;
			orientation-switch = <&it5205fn>;
			mode-switch = <&it5205fn>;
			displayport = <&dp_intf1>;

			altmodes {
				dp {
					svid = <0x0ff01>;
					vdo = <0x1c1c47>;
				};
			};

			ports {
				#address-cells = <1>;
				#size-cells = <0>;

				port@0 {
					reg = <0>;
					tcpc_typec_usb: endpoint {
							remote-endpoint = <&ssusb1_ep>;
					};
				};
			};
		};

		ports {
			#address-cells = <1>;
			#size-cells = <0>;

			port@0 {};
		};
	};
};

&i2c5 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c5_pin>;
	clock-frequency = <400000>;
	status = "okay";
};

&i2c6 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c6_pin>;
	clock-frequency = <400000>;
	status = "okay";
};

&pmic {
	interrupt-parent = <&pio>;
	interrupts = <222 IRQ_TYPE_LEVEL_HIGH>;

	pmic_key: pmic-key {
		compatible = "mediatek,mt6359-keys";
		mediatek,long-press-mode = <1>;
		power-off-time-sec = <0>;

		power-key{
			linux,keycodes = <116>;
			wakeup-source;
		};

		home-key{
			linux,keycodes = <114>;
		};
	};
};

&scp {
	memory-region = <&scp_mem_reserved>;
	status = "okay";
};

&keypad {
	mediatek,debounce-us = <32000>;
	mediatek,double-keys;
	keypad,num-rows = <1>;
	keypad,num-columns = <2>;
	linux,keymap =
		<MATRIX_KEY(0x00, 0x00, KEY_VOLUMEDOWN)
		 MATRIX_KEY(0x00, 0x01, KEY_VOLUMEUP)>;
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&kpd_gpios_def_cfg>;
};

&mt6359codec {
	mediatek,mic-type-0 = <1>; /* ACC */
	mediatek,mic-type-1 = <3>; /* DCC */
};

&mt6359_vbbck_ldo_reg {
	regulator-always-on;
};

&mt6359_vcn33_2_bt_ldo_reg {
	regulator-always-on;
};

&mt6359_vcore_buck_reg {
	regulator-always-on;
};

&mt6359_vgpu11_buck_reg {
	regulator-always-on;
};

&mt6359_vpu_buck_reg {
	regulator-always-on;
};

&mt6359_vrf12_ldo_reg {
	regulator-always-on;
};

&mt6359_vufs_ldo_reg {
	regulator-always-on;
};

&uart0 {
	pinctrl-0 = <&uart0_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&uart1 {
	pinctrl-0 = <&uart1_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&uart2 {
	pinctrl-0 = <&uart2_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&mipi_tx0 {
	status = "okay";
};

&mmc0 {
	status = "okay";
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc0_pins_default>;
	pinctrl-1 = <&mmc0_pins_uhs>;
	bus-width = <8>;
	max-frequency = <200000000>;
	cap-mmc-highspeed;
	mmc-hs200-1_8v;
	mmc-hs400-1_8v;
	supports-cqe;
	cap-mmc-hw-reset;
	no-sdio;
	no-sd;
	hs400-ds-delay = <0x1481b>;
	vmmc-supply = <&mt6359_vemc_1_ldo_reg>;
	vqmmc-supply = <&mt6359_vufs_ldo_reg>;
	non-removable;
};

&mmc1 {
	status = "okay";
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc1_pins_default>;
	pinctrl-1 = <&mmc1_pins_uhs>;
	bus-width = <4>;
	max-frequency = <200000000>;
	cap-sd-highspeed;
	sd-uhs-sdr50;
	sd-uhs-sdr104;
	no-mmc;
	no-sdio;
	cd-gpios = <&pio 2 GPIO_ACTIVE_LOW>;
	vmmc-supply = <&mt6359_vpa_buck_reg>;
	vqmmc-supply = <&mt6359_vsim1_ldo_reg>;
};

&mt6359_vpa_buck_reg {
	regulator-max-microvolt = <3100000>;
};

&mt6359_vsim1_ldo_reg {
	regulator-enable-ramp-delay = <480>;
};

&wifi_pwrseq {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&wifi_pins_pwrseq>;

	reset-gpios = <&pio 127 1>;
	post-power-on-delay-ms = <200>;
};

&mmc2 {
	status = "okay";
	pinctrl-names = "default", "state_uhs", "state_eint",
					"state_dat1";
	pinctrl-0 = <&mmc2_pins_default>;
	pinctrl-1 = <&mmc2_pins_uhs>;
	pinctrl-2 = <&mmc2_pins_eint>;
	pinctrl-3 = <&mmc2_pins_dat1>;
	eint-gpios = <&pio 172 0>;
	bus-width = <4>;
	max-frequency = <200000000>;
	cap-sd-highspeed;
	sd-uhs-sdr104;
	keep-power-in-suspend;
	enable-sdio-wakeup;
	cap-sdio-async-int;
	cap-sdio-irq;
	no-mmc;
	no-sd;
	non-removable;
	vmmc-supply = <&sdio_fixed_3v3>;
	vqmmc-supply = <&sdio_fixed_1v8>;
	mmc-pwrseq = <&wifi_pwrseq>;
};

&mt6359_vcn18_ldo_reg {
	regulator-always-on;
};

&spi2 {
	pinctrl-0 = <&spi2_pins>;
	pinctrl-names = "default";
	mediatek,pad-select = <0>;
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	spidev@0 {
		compatible = "mediatek,aiot-board";
		spi-max-frequency = <5000000>;
		reg = <0>;
	};
};

&spmi {
	mt6315_6: mt6315@6 {
		compatible = "mediatek,mt6315-regulator";
		reg = <0x6 0 0xb 1>;

		regulators {
			mt6315_6_vbuck1: vbuck1 {
				regulator-compatible = "vbuck1";
				regulator-name = "Vbcpu";
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1193750>;
				regulator-enable-ramp-delay = <256>;
				regulator-allowed-modes = <0 1 2>;
				regulator-always-on;
				mtk,combined-regulator = <2>;
			};
			mt6315_6_vbuck3: vbuck3 {
				regulator-compatible = "vbuck3";
				regulator-name = "Vdd2";
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1193750>;
				regulator-enable-ramp-delay = <256>;
				regulator-allowed-modes = <0 1 2>;
				regulator-always-on;
			};
			mt6315_6_vbuck4: vbuck4 {
				regulator-compatible = "vbuck4";
				regulator-name = "Vddq";
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1193750>;
				regulator-enable-ramp-delay = <256>;
				regulator-allowed-modes = <0 1 2>;
				regulator-always-on;
			};
		};
	};
};

&pcie {
	pinctrl-names = "default";
	pinctrl-0 = <&pcie_pins_default>;
	status = "okay";
};

&pciephy {
	status = "okay";
};

&ssusb {
	pinctrl-0 = <&usb_default>;
	pinctrl-names = "default";
	maximum-speed = "high-speed";
	usb-role-switch;
	dr_mode = "otg";
	vusb33-supply = <&mt6359_vusb_ldo_reg>;
	status = "okay";
	wakeup-source;

	connector {
		compatible = "gpio-usb-b-connector", "usb-b-connector";
		type = "micro";
		id-gpios = <&pio 83 GPIO_ACTIVE_HIGH>;
		vbus-supply = <&usb_p0_vbus>;
	};
};

&u2port0 {
	status = "okay";
};

&u3phy0 {
	status = "okay";
};

&usb_host0 {
	status = "okay";
	vusb33-supply = <&mt6359_vusb_ldo_reg>;
};

&ssusb1 {
	pinctrl-0 = <&usb1_default>;
	pinctrl-names = "default";
	vusb33-supply = <&mt6359_vusb_ldo_reg>;
	dr_mode = "host";
	mediatek,usb3-drd;
	usb-role-switch;
	status = "okay";
	wakeup-source;

	port {
		ssusb1_ep: endpoint {
			remote-endpoint = <&tcpc_typec_usb>;
		};
	};
};

&u2port1 {
	status = "okay";
};

&u3port1 {
	status = "okay";
};

&u3phy1 {
	status = "okay";
};

&usb_host1 {
	status = "okay";
	vusb33-supply = <&mt6359_vusb_ldo_reg>;
	vbus-supply = <&onboard_hub>;
};

&ssusb2 {
	maximum-speed = "high-speed";
	usb-role-switch;
	dr_mode = "host";
	vusb33-supply = <&mt6359_vusb_ldo_reg>;
	status = "okay";
	wakeup-source;

	connector {
		compatible = "gpio-usb-b-connector", "usb-b-connector";
		type = "micro";
		id-gpios = <&pio 89 GPIO_ACTIVE_HIGH>;
		vbus-supply = <&usb_p2_vbus>;
	};
};

&u2port2 {
	status = "okay";
};

&u3phy2 {
	status = "okay";
};

&usb_host2 {
	status = "okay";
	vusb33-supply = <&mt6359_vusb_ldo_reg>;
};

&mfg0 {
	domain-supply = <&mt6359_vproc2_buck_reg>;
};

&sound {
	compatible = "mediatek,mt8390-evk";
	model = "mt8390-evk";
	pinctrl-names = "default";
	pinctrl-0 = <&aud_pins_default>;
	audio-routing =
		"Headphone", "Headphone L",
		"Headphone", "Headphone R",
		"AIN1", "Headset Mic",
		"DMIC_INPUT", "AP DMIC",
		"AP DMIC", "AUDGLB",
		"AP DMIC", "MIC_BIAS_0",
		"AP DMIC", "MIC_BIAS_2";
	status = "okay";

	dai-link-0 {
		sound-dai = <&afe>;
		dai-link-name = "ADDA_BE";
		status = "okay";

		codec-0 {
			sound-dai = <&pmic 0>;
		};

		codec-1 {
			sound-dai = <&amic_codec>;
		};
	};

	dai-link-1 {
		sound-dai = <&afe>;
		dai-link-name = "ETDM3_OUT_BE";
		status = "okay";

		codec-0 {
			sound-dai = <&hdmi0>;
		};
	};

	dai-link-2 {
		sound-dai = <&afe>;
		dai-link-name = "DPTX_BE";
		status = "disabled";

		codec-0 {
			sound-dai = <&dp_tx>;
		};
	};

	dai-link-3 {
		sound-dai = <&afe>;
		dai-link-name = "DMIC_BE";
		status = "okay";

		codec-0 {
			sound-dai = <&dmic_codec>;
		};
	};
};

&cam_vcore {
	domain-supply = <&mt6359_vproc1_buck_reg>;
};

&img_vcore {
	domain-supply = <&mt6359_vproc1_buck_reg>;
};
