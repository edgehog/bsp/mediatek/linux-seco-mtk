/*
* Copyright 2024 SECO Northern Europe GmbH
*/

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/interrupt-controller/irq.h>
#include <dt-bindings/pinctrl/mediatek,mt8188-pinfunc.h>
#include <dt-bindings/usb/pd.h>
#include "seco-genio-pinfunc.h"

/ {	
	aliases {
		serial0 = &uart0;
		serial1 = &uart1;
		dsi0 = &dsi0;
		ethernet0 = &eth;
	};
	
	backlight_lcd0: backlight-lcd0 {
		compatible = "pwm-backlight";
		pwms = <&disp_pwm0 0 500000>;
		enable-gpios = <LVDS0_BKLT_EN GPIO_ACTIVE_HIGH>;
		brightness-levels = <0 1023>;
		num-interpolated-steps = <1023>;
		default-brightness-level = <576>;
		status = "disabled";
	};

	backlight_lcd1: backlight-lcd1 {
		compatible = "pwm-backlight";
		pwms = <&disp_pwm1 0 500000>;
		enable-gpios = <LCD1_BKLT_EN GPIO_ACTIVE_HIGH>;
		brightness-levels = <0 1023>;
		num-interpolated-steps = <1023>;
		default-brightness-level = <576>;
		status = "disabled";
	};
	
	chosen {
		stdout-path = "serial1:115200n8";
	};
	
	memory@40000000 {
		device_type = "memory";
		reg = <0 0x40000000 1 0x00000000>;
	};
	
	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;
		
		apu_reserve_memory: apu-reserve-memory{
			compatible = "shared-dma-pool";
			size = <0 0x1400000>;//20 MB
			alignment = <0 0x10000>;
			reg = <0 0x55000000 0 0x1400000>;
		};
		
		vpu_reserve_memory: vpu-reserve-memory {
			compatible = "shared-dma-pool";
			size = <0 0x1400000>;//20 MB
			alignment = <0 0x10000>;
			reg = <0 0x57000000 0 0x1400000>;
		};
		
		/* 12 MiB reserved for OP-TEE (BL32)
		* +-----------------------+ 0x43e0_0000
		* |      SHMEM 2MiB       |
		* +-----------------------+ 0x43c0_0000
		* |        | TA_RAM  8MiB |
		* + TZDRAM +--------------+ 0x4340_0000
		* |        | TEE_RAM 2MiB |
		* +-----------------------+ 0x4320_0000
		*/
		optee_reserved: optee@43200000 {
			no-map;
			reg = <0 0x43200000 0 0x00c00000>;
		};
		
		scp_mem_reserved: scp-mem-region {
			compatible = "shared-dma-pool";
			reg = <0 0x50000000 0 0x2900000>;
			no-map;
		};
		
		/* 2 MiB reserved for ARM Trusted Firmware (BL31) */
		bl31_secmon_reserved: secmon@54600000 {
			no-map;
			reg = <0 0x54600000 0x0 0x200000>;
		};
	};
	
	firmware {
		optee {
			compatible = "linaro,optee-tz";
			method = "smc";
		};
	};
	
	// *******************
	// regulators for wilk
	// from baseboard
	reg_5v_sleep: 5v_sleep {
		compatible = "regulator-fixed";
		regulator-name = "5V_SLEEP";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
	};
	
	// voltage regulator u2300
	reg_3v3_sleep: 3v3_sleep {
		compatible = "regulator-fixed";
		regulator-name = "3V3_SLEEP";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		vin-supply = <&reg_5v_sleep>;
		regulator-always-on;
	};
	
	// voltage regulator u2301
	reg_1v8_sleep: 1v8_sleep {
		compatible = "regulator-fixed";
		regulator-name = "1V8_SLEEP";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&reg_3v3_sleep>;
		regulator-always-on;
	};
	
	// voltage switch u2302
	reg_5v_run: 5v_run {
		compatible = "regulator-fixed";
		regulator-name = "5V_RUN";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		vin-supply = <&reg_5v_sleep>;
		regulator-always-on;
	};
	
	// MT6365 BUCK
	reg_dvdd_mm: dvdd_mm {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_MM";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vproc1_buck_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_gpu: dvdd_gpu {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_GPU";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vproc2_buck_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_proc_l: dvdd_proc_l {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_PROC_L";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vcore_buck_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_adsp: dvdd_adsp {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_ADSP";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vpu_buck_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_sram_core: dvdd_sram_core {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_SRAM_CORE";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vpu_buck_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_sram_mm: dvdd_sram_mm {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_SRAM_MM";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vpu_buck_reg>;
		regulator-always-on;
	};
	
	reg_avdd075_drv_dsi: avdd075_drv_dsi {
		compatible = "regulator-fixed";
		regulator-name = "AVDD075_DRV_DSI";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vpu_buck_reg>;
		regulator-always-on;
	};
	
	reg_3v3_wifi: 3v3_wifi {
		compatible = "regulator-fixed";
		regulator-name = "3V3_WIFI";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		vin-supply = <&mt6359_vpa_buck_reg>;
		regulator-always-on;
	};
	
	reg_vs1_pmu: vs1_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VS1_PMU";
		regulator-min-microvolt = <2000000>;
		regulator-max-microvolt = <2000000>;
		vin-supply = <&mt6359_vs1_buck_reg>;
		regulator-always-on;
	};
	
	reg_vs2_pmu: vs2_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VS2_PMU";
		regulator-min-microvolt = <1350000>;
		regulator-max-microvolt = <1350000>;
		vin-supply = <&mt6359_vs2_buck_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_apu: dvdd_apu {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_APU";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vmodem_buck_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_core: dvdd_core {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_CORE";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vgpu11_buck_reg>;
		regulator-always-on;
	};
	
	// MT6365 LDO
	reg_vaux18_pmu: vaux18_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VAUX18_PMU";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_vaux18_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vusb_pmu: vusb_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VUSB_PMU";
		regulator-min-microvolt = <3000000>;
		regulator-max-microvolt = <3000000>;
		vin-supply = <&mt6359_vusb_ldo_reg>;
		regulator-always-on;
	};
	
	reg_3v3_run: 3v3_run {
		compatible = "regulator-fixed";
		regulator-name = "3V3_RUN";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		vin-supply = <&mt6359_vcn33_1_bt_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vemc_pmu: vemc_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VEMC_PMU";
		regulator-min-microvolt = <3000000>;
		regulator-max-microvolt = <3000000>;
		vin-supply = <&mt6359_vemc_1_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vsim1_pmu: vsim1_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VSIM1_PMU";
		regulator-min-microvolt = <2800000>;
		regulator-max-microvolt = <2800000>;
		vin-supply = <&mt6359_vsim1_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vio28_pmu: vio28_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VIO28_PMU";
		regulator-min-microvolt = <2800000>;
		regulator-max-microvolt = <2800000>;
		vin-supply = <&mt6359_vio18_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vufs18_pmu: vufs18_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VUFS18_PMU";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_vufs_ldo_reg>;
		regulator-always-on;
	};
	
	reg_1v8_run: 1v8_run {
		compatible = "regulator-fixed";
		regulator-name = "1V8_RUN";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_vcn18_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vio18_pmu:  vio18_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VIO18_PMU";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_vio18_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vio18_pmu_avdd:  vio18_pmu_avdd {
		compatible = "regulator-fixed";
		regulator-name = "VIO18_PMU_AVDD";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_vio18_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vefuse_pmu:  vefuse_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VEFUSE_PMU";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_vefuse_ldo_reg>;
		regulator-always-on;
	};
	
	reg_vaud18_pmu:  vaud18_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VAUD18_PMU";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_vaud18_ldo_reg>;
		regulator-always-on;
	};
	
	reg_va12_abb1_pmu:  va12_abb1_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VA12_ABB1_PMU";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_va12_ldo_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_sram_apu: dvdd_sram_apu {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_SRAM_APU";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vsram_md_ldo_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_sram_proc_l: dvdd_sram_proc_l {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_SRAM_PROC_L";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vsram_proc1_ldo_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_sram_proc_b: dvdd_sram_proc_b {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_SRAM_PROC_B";
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&mt6359_vsram_proc2_ldo_reg>;
		regulator-always-on;
	};
	
	reg_dvdd_sram_gpu: dvdd_sram_gpu {
		compatible = "regulator-fixed";
		regulator-name = "DVDD_SRAM_GPU";
		regulator-min-microvolt = <850000>;
		regulator-max-microvolt = <850000>;
		vin-supply = <&mt6359_vsram_others_ldo_reg>;
		regulator-always-on;
	};
	
	reg_va12_abb2_pmu:  va12_abb2_pmu {
		compatible = "regulator-fixed";
		regulator-name = "VA12_ABB2_PMU";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&mt6359_vrf12_ldo_reg>;
		regulator-always-on;
	};

	can_clk: can-clk {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <20000000>;
		clock-output-names = "can-clk";
	};

	lvds_panel: lvds-panel {
		compatible = "seco,panel-lvds";
		status = "disabled";

		ports {
			#address-cells = <1>;
			#size-cells = <0>;

			port@0 {
				reg = <0>;
				panel0_in_ch0: endpoint {
					remote-endpoint = <&lvds_bridge_out_ch0>;
				};
			};
		};
	};
};

&afe {
	#sound-dai-cells = <0>;
	mediatek,dmic-iir-on;
	mediatek,dmic-clk-mono;
	status = "okay";
};

&eth {
	phy-mode ="rgmii-rxid";
	phy-handle = <&ethernet_phy0>;
	snps,reset-gpio = <ENET0_RST_1V8 GPIO_ACTIVE_HIGH>;
	snps,reset-delay-us = <0 10000 10000>;
	mediatek,tx-delay-ps = <2030>;
	mediatek,mac-wol;
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&eth_default_pins>;
	pinctrl-1 = <&eth_sleep_pins>;
	status = "okay";
	
	mdio {
		compatible = "snps,dwmac-mdio";
		#address-cells = <1>;
		#size-cells = <0>;
		ethernet_phy0: ethernet-phy@0 {
			compatible = "ethernet-phy-id001c.c916";
			reg = <0>;
		};
	};
};

&dpi1 {
	status = "okay";
};

&disp_pwm0 {
	pinctrl-names = "default";
	pinctrl-0 = <&disp_pwm0_pin_default>;
	status = "okay";
};

&dp_intf1 {
	status = "disabled";
	mediatek,oob-hpd;
	ports {
		port {
			dp_intf1_out: endpoint {
				remote-endpoint = <&dptx_in>;
			};
		};
	};
};

&dp_tx {
	pinctrl-names = "default";
	pinctrl-0 = <&dptx_pin>;
	status = "disabled";
	
	ports {
		#address-cells = <1>;
		#size-cells = <0>;
		port {
			dptx_in: endpoint {
				remote-endpoint = <&dp_intf1_out>;
			};
		};
	};
};

&hdmi0 {
	status = "okay";
};

&dsi0 {
	ports {
		port {
			dsi0_out: endpoint {
				remote-endpoint = <&lvds_bridge_in>;
			};
		};
	};
};

&pio {
	gpio-line-names =
		"GPIO6", "GPIO_10", "SDIO_CD#", "WIFI_DISABLE", 
		"SDIO_PWR_EN", "BT_IRQ", "TPM_RST#", "I2C_PIRQ#", 
		"", "GPIO7", "GPIO8", "GPIO9",
		"LVDS_EN", "EC_IRQ#", "", "CPU_IRQ#", 
		
		"MT6319_INT", "EDP1_HPD", "GPIO0/CAM0_PWR#", "GPIO1/CAM1_PWR#", 
		"GPIO2/CAM0_RST#", "GPIO3/CAM1_RST#", "CAM_MCK", "", 
		"", "LVDS0_BKLT_EN", "LVDS0_VDD_EN", "LCD1_BKLT_EN", 
		"LCD1_VDD_EN", "LVDS0_BKLT_PWM", "LCD1_BKLT_PWM", "SER1_TX", 
		
		"SER1_RX", "SER0_TX", "SER0_RX", "SER0_RTS#", 
		"SER0_CTS#", "JTAG_TMS", "JTAG_TCK", "JTAG_TDI", 
		"JTAG_TDO", "JTAG_TRST#", "KPCOL0", "KPCOL1", 
		"KPROW0", "KPROW1", "DP0_HPD", "PCIE_WAKE#",
		
		"PCIE_A_RST#", "PCIE_A_CKREQ#", "", "HDMI_HPD", 
		"", "HDMI_CTRL_CK", "HDMI_CTRL_DAT", "I2C0_SCL", 
		"I2C0_SDA", "I2C1_SCL", "I2C1_SDA", "I2C2_SCL", 
		"I2C2_SDA", "I2C3_SCL", "I2C3_SDA", "I2C_GP_CK", 
		
		"I2C_GP_DAT", "I2C5_SCL", "I2C5_SDA", "I2C_PM_CK", 
		"I2C_PM_DAT", "CAN_CS#", "CAN_SCK", "CAN_SDI", 
		"CAN_SDO", "CAN_INT", "", "SPI0_CS0#",
		"SPI0_CK", "SPI0_DO", "SPI0_DIN", "UART2_TX", 
		
		"UART2_RX", "UART2_RTS#", "UART2_CTS#", "USB0_OTG_ID", 
		"", "", "", "",
		"USB3_VBUS_1V8", "", "", "", 
		"PWRAP_SPI_CSN", "PWRAP_SPI_CK", "PWRAP_SPI_MO", "PWRAP_SPI_MI",
		
		"PMIC_SRCLKENA0", "PMIC_SRCLKENA1", "SCP_VREQ_VAO", "PMIC_RTC32K_CK",
		"PMIC_WATCHDOG#", "AUD_CLK_MOSI", "AUD_SYNC_MOSI", "AUD_DAT_MOSI0", 
		"AUD_DAT_MOSI1", "AUD_DAT_MISO0", "AUD_DAT_MISO1", "", 
		"I2S2_CK", "I2S2_LRCK", "I2S0_SDIN", "I2S2_SDIN", 
		
		"", "", "I2SO2_MCK", "I2S0_CK", 
		"I2S0_LRCK", "I2S0_SDOUT", "I2S2_SDOUT", "",
		"", "PCM_CLK", "PCM_SYNC", "PCM_DO", 
		"PCM_DI", "ESPI_CK", "ESPI_CS0#", "ESPI_IO_0", 
		
		"ESPI_IO_1", "ESPI_IO_2", "ESPI_IO_3", "ENET0_TD3",
		"ENET0_TD2", "ENET0_TD1", "ENET0_TD0", "ENET0_RD3", 
		"ENET0_RD2", "ENET0_RD1", "ENET0_RD0", "ENET0_TXC", 
		"ENET0_RXC", "ENET0_RX_CTL", "ENET0_TX_CTL", "ENET0_MDC", 
		
		"ENET0_MDIO", "", "", "ENET0_RST_1V8",
		"ENET0_INT#_1V8", "SER3_TX", "SER3_RX", "EMMC_DAT7", 
		"EMMC_DAT6", "EMMC_DAT5", "EMMC_DAT4", "EMMC_RST#", 
		"EMMC_CMD", "EMMC_CLK", "EMMC_DAT3", "EMMC_DAT2", 
		
		"EMMC_DAT1", "EMMC_DAT0", "EMMC_DSL", "SDIO_CMD", 
		"SDIO_CK", "SDIO_D0", "SDIO_D1", "SDIO_D2", 
		"SDIO_D3", "MSDC2_CMD", "MSDC2_CLK", "MSDC2_DAT0", 
		"MSDC2_DAT1", "MSDC2_DAT2", "MSDC2_DAT3", "SPMI_M_SCL", 
		
		"SPMI_M_SDA";
};

&pio {
	aud_pins_default: audiodefault {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO101__FUNC_O_AUD_CLK_MOSI>,
			<PINMUX_GPIO102__FUNC_O_AUD_SYNC_MOSI>,
			<PINMUX_GPIO103__FUNC_O_AUD_DAT_MOSI0>,
			<PINMUX_GPIO104__FUNC_O_AUD_DAT_MOSI1>,
			<PINMUX_GPIO105__FUNC_I0_AUD_DAT_MISO0>,
			<PINMUX_GPIO106__FUNC_I0_AUD_DAT_MISO1>,
			<PINMUX_GPIO107__FUNC_B0_I2SIN_MCK>,
			<PINMUX_GPIO108__FUNC_B0_I2SIN_BCK>,
			<PINMUX_GPIO109__FUNC_B0_I2SIN_WS>,
			<PINMUX_GPIO110__FUNC_I0_I2SIN_D0>,
			<PINMUX_GPIO114__FUNC_O_I2SO2_MCK>,
			<PINMUX_GPIO115__FUNC_B0_I2SO2_BCK>,
			<PINMUX_GPIO116__FUNC_B0_I2SO2_WS>,
			<PINMUX_GPIO117__FUNC_O_I2SO2_D0>,
			<PINMUX_GPIO118__FUNC_O_I2SO2_D1>,
			<PINMUX_GPIO125__FUNC_O_DMIC1_CLK>,
			<PINMUX_GPIO126__FUNC_I0_DMIC1_DAT>,
			<PINMUX_GPIO128__FUNC_O_DMIC2_CLK>,
			<PINMUX_GPIO129__FUNC_I0_DMIC2_DAT>,
			<PINMUX_GPIO121__FUNC_B0_PCM_CLK>,
			<PINMUX_GPIO122__FUNC_B0_PCM_SYNC>,
			<PINMUX_GPIO124__FUNC_I0_PCM_DI>;
		};
	};
	
	edp_panel_3v3_en_pins: edp-panel-3v3-en-pins {
		pins1 {
			pinmux = <PINMUX_GPIO15__FUNC_B_GPIO15>;
			output-high;
		};
	};
	
	dptx_pin: dptx-pin {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO46__FUNC_I0_DP_TX_HPD>;
			bias-pull-up;
		};
	};
	
	eth_default_pins: eth-default-pins {
		pins-txd {
			pinmux = <PINMUX_GPIO131__FUNC_O_GBE_TXD3>,
			<PINMUX_GPIO132__FUNC_O_GBE_TXD2>,
			<PINMUX_GPIO133__FUNC_O_GBE_TXD1>,
			<PINMUX_GPIO134__FUNC_O_GBE_TXD0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
		pins-cc {
			pinmux = <PINMUX_GPIO139__FUNC_B0_GBE_TXC>,
			<PINMUX_GPIO142__FUNC_O_GBE_TXEN>,
			<PINMUX_GPIO141__FUNC_I0_GBE_RXDV>,
			<PINMUX_GPIO140__FUNC_I0_GBE_RXC>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
		pins-rxd {
			pinmux = <PINMUX_GPIO135__FUNC_I0_GBE_RXD3>,
			<PINMUX_GPIO136__FUNC_I0_GBE_RXD2>,
			<PINMUX_GPIO137__FUNC_I0_GBE_RXD1>,
			<PINMUX_GPIO138__FUNC_I0_GBE_RXD0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
		pins-mdio {
			pinmux = <PINMUX_GPIO143__FUNC_O_GBE_MDC>,
			<PINMUX_GPIO144__FUNC_B1_GBE_MDIO>;
			drive-strength = <MTK_DRIVE_8mA>;
			input-enable;
		};
		pins-power {
			pinmux = <PINMUX_GPIO145__FUNC_B_GPIO145>,
			<PINMUX_GPIO146__FUNC_B_GPIO146>;
			output-high;
		};
		pins-reset {
			pinmux = <PINMUX_GPIO147__FUNC_B_GPIO147>;
			output-low;
		};
	};
	
	eth_sleep_pins: eth-sleep-pins {
		pins-txd {
			pinmux = <PINMUX_GPIO131__FUNC_B_GPIO131>,
			<PINMUX_GPIO132__FUNC_B_GPIO132>,
			<PINMUX_GPIO133__FUNC_B_GPIO133>,
			<PINMUX_GPIO134__FUNC_B_GPIO134>;
		};
		pins-cc {
			pinmux = <PINMUX_GPIO139__FUNC_B_GPIO139>,
			<PINMUX_GPIO142__FUNC_B_GPIO142>,
			<PINMUX_GPIO141__FUNC_B_GPIO141>,
			<PINMUX_GPIO140__FUNC_B_GPIO140>;
		};
		pins-rxd {
			pinmux = <PINMUX_GPIO135__FUNC_B_GPIO135>,
			<PINMUX_GPIO136__FUNC_B_GPIO136>,
			<PINMUX_GPIO137__FUNC_B_GPIO137>,
			<PINMUX_GPIO138__FUNC_B_GPIO138>;
		};
		pins-mdio {
			pinmux = <PINMUX_GPIO143__FUNC_B_GPIO143>,
			<PINMUX_GPIO144__FUNC_B_GPIO144>;
			input-disable;
			bias-disable;
		};
	};
	
	uart0_pins: uart0-pins {
		pins {
			pinmux = <PINMUX_GPIO31__FUNC_O_UTXD0>,
			<PINMUX_GPIO32__FUNC_I1_URXD0>;
			bias-pull-up;
		};
	};
	
	uart1_pins: uart1-pins {
		pins {
			pinmux = <PINMUX_GPIO33__FUNC_O_UTXD1>,
			<PINMUX_GPIO34__FUNC_I1_URXD1>,
			<PINMUX_GPIO35__FUNC_O_URTS1>,
			<PINMUX_GPIO36__FUNC_I1_UCTS1>;
			bias-pull-up;
		};
	};
	
	uart2_pins: uart2-pins {
		pins {
			pinmux = <PINMUX_GPIO79__FUNC_O_UTXD2>,
			<PINMUX_GPIO80__FUNC_I1_URXD2>,
			<PINMUX_GPIO81__FUNC_O_URTS2>,
			<PINMUX_GPIO82__FUNC_I1_UCTS2>;
			bias-pull-up;
		};
	};
	
	uart3_pins: uart3-pins {
		pins {
			pinmux = <PINMUX_GPIO149__FUNC_O_UTXD3>,
			<PINMUX_GPIO150__FUNC_I1_URXD3>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};
	
	i2c0_pin: i2c0-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO56__FUNC_B1_SDA0>,
			<PINMUX_GPIO55__FUNC_B1_SCL0>;
			bias-pull-up = <MTK_PULL_SET_RSEL_101>;
			mediatek,drive-strength-adv = <0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
	};
	
	i2c1_pin: i2c1-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO58__FUNC_B1_SDA1>,
			<PINMUX_GPIO57__FUNC_B1_SCL1>;
			bias-pull-up = <MTK_PULL_SET_RSEL_101>;
			mediatek,drive-strength-adv = <0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
	};
	
	i2c2_pin: i2c2-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO60__FUNC_B1_SDA2>,
			<PINMUX_GPIO59__FUNC_B1_SCL2>;
			bias-pull-up = <MTK_PULL_SET_RSEL_101>;
			mediatek,drive-strength-adv = <0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
	};
	
	i2c3_pin: i2c3-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO62__FUNC_B1_SDA3>,
			<PINMUX_GPIO61__FUNC_B1_SCL3>;
			bias-pull-up = <MTK_PULL_SET_RSEL_101>;
			mediatek,drive-strength-adv = <0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
	};
	
	i2c4_pin: i2c4-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO64__FUNC_B1_SDA4>,
			<PINMUX_GPIO63__FUNC_B1_SCL4>;
			bias-pull-up = <MTK_PULL_SET_RSEL_111>;
			mediatek,drive-strength-adv = <0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
	};
	
	i2c5_pin: i2c5-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO66__FUNC_B1_SDA5>,
			<PINMUX_GPIO65__FUNC_B1_SCL5>;
			bias-pull-up = <MTK_PULL_SET_RSEL_101>;
			mediatek,drive-strength-adv = <0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
	};
	
	i2c6_pin: i2c6-pin {
		pins-bus {
			pinmux = <PINMUX_GPIO68__FUNC_B1_SDA6>,
			<PINMUX_GPIO67__FUNC_B1_SCL6>;
			bias-pull-up = <MTK_PULL_SET_RSEL_101>;
			mediatek,drive-strength-adv = <0>;
			drive-strength = <MTK_DRIVE_8mA>;
		};
	};

	kpd_gpios_def_cfg: keypad-pins-default {
		pins_kp {
			pinmux = <PINMUX_GPIO44__FUNC_B1_KPROW0>,
			<PINMUX_GPIO43__FUNC_B1_KPCOL1>,
			<PINMUX_GPIO42__FUNC_B1_KPCOL0>;
		};
	};
	
	mmc0_pins_default: mmc0default {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO161__FUNC_B1_MSDC0_DAT0>,
			<PINMUX_GPIO160__FUNC_B1_MSDC0_DAT1>,
			<PINMUX_GPIO159__FUNC_B1_MSDC0_DAT2>,
			<PINMUX_GPIO158__FUNC_B1_MSDC0_DAT3>,
			<PINMUX_GPIO154__FUNC_B1_MSDC0_DAT4>,
			<PINMUX_GPIO153__FUNC_B1_MSDC0_DAT5>,
			<PINMUX_GPIO152__FUNC_B1_MSDC0_DAT6>,
			<PINMUX_GPIO151__FUNC_B1_MSDC0_DAT7>,
			<PINMUX_GPIO156__FUNC_B1_MSDC0_CMD>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
		
		pins-clk {
			pinmux = <PINMUX_GPIO157__FUNC_B1_MSDC0_CLK>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};
		
		pins-rst {
			pinmux = <PINMUX_GPIO155__FUNC_O_MSDC0_RSTB>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};
	
	mmc0_pins_uhs: mmc0uhs{
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO161__FUNC_B1_MSDC0_DAT0>,
			<PINMUX_GPIO160__FUNC_B1_MSDC0_DAT1>,
			<PINMUX_GPIO159__FUNC_B1_MSDC0_DAT2>,
			<PINMUX_GPIO158__FUNC_B1_MSDC0_DAT3>,
			<PINMUX_GPIO154__FUNC_B1_MSDC0_DAT4>,
			<PINMUX_GPIO153__FUNC_B1_MSDC0_DAT5>,
			<PINMUX_GPIO152__FUNC_B1_MSDC0_DAT6>,
			<PINMUX_GPIO151__FUNC_B1_MSDC0_DAT7>,
			<PINMUX_GPIO156__FUNC_B1_MSDC0_CMD>;
			input-enable;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
		
		pins-clk {
			pinmux = <PINMUX_GPIO157__FUNC_B1_MSDC0_CLK>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};
		
		pins-ds {
			pinmux = <PINMUX_GPIO162__FUNC_B0_MSDC0_DSL>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};
		
		pins-rst {
			pinmux = <PINMUX_GPIO155__FUNC_O_MSDC0_RSTB>;
			drive-strength = <MTK_DRIVE_8mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};
	
	mmc1_pins_default: mmc1default {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO163__FUNC_B1_MSDC1_CMD>,
			<PINMUX_GPIO165__FUNC_B1_MSDC1_DAT0>,
			<PINMUX_GPIO166__FUNC_B1_MSDC1_DAT1>,
			<PINMUX_GPIO167__FUNC_B1_MSDC1_DAT2>,
			<PINMUX_GPIO168__FUNC_B1_MSDC1_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
		
		pins-clk {
			pinmux = <PINMUX_GPIO164__FUNC_B1_MSDC1_CLK>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};
		
		pins-insert {
			pinmux = <PINMUX_GPIO2__FUNC_B_GPIO2>;
			bias-pull-up;
		};
		
		pins-enable {
			pinmux = <PINMUX_GPIO4__FUNC_B_GPIO4>;
			bias-pull-up;
		};
	};
	
	mmc1_pins_uhs: mmc1uhs {
		pins-cmd-dat {
			pinmux = <PINMUX_GPIO163__FUNC_B1_MSDC1_CMD>,
			<PINMUX_GPIO165__FUNC_B1_MSDC1_DAT0>,
			<PINMUX_GPIO166__FUNC_B1_MSDC1_DAT1>,
			<PINMUX_GPIO167__FUNC_B1_MSDC1_DAT2>,
			<PINMUX_GPIO168__FUNC_B1_MSDC1_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
		
		pins-clk {
			pinmux = <PINMUX_GPIO164__FUNC_B1_MSDC1_CLK>;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};
		
		pins-insert {
			pinmux = <PINMUX_GPIO2__FUNC_B_GPIO2>;
			bias-pull-up;
		};
		
		pins-enable {
			pinmux = <PINMUX_GPIO4__FUNC_B_GPIO4>;
			bias-pull-up;
		};
	};
	
	mmc2_pins_default: mmc2default {
		pins-clk {
			pinmux = <PINMUX_GPIO170__FUNC_B1_MSDC2_CLK>;
			drive-strength = <MTK_DRIVE_4mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins-cmd-dat {
			pinmux = <PINMUX_GPIO169__FUNC_B1_MSDC2_CMD>,
				<PINMUX_GPIO171__FUNC_B1_MSDC2_DAT0>,
				<PINMUX_GPIO172__FUNC_B1_MSDC2_DAT1>,
				<PINMUX_GPIO173__FUNC_B1_MSDC2_DAT2>,
				<PINMUX_GPIO174__FUNC_B1_MSDC2_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};

		pins-pcm {
			pinmux = <PINMUX_GPIO123__FUNC_O_PCM_DO>;
		};
	};
	
	mmc2_pins_uhs: mmc2uhs {
		pins-clk {
			pinmux = <PINMUX_GPIO170__FUNC_B1_MSDC2_CLK>;
			drive-strength = <MTK_DRIVE_4mA>;
			bias-pull-down = <MTK_PUPD_SET_R1R0_10>;
		};

		pins-cmd-dat {
			pinmux = <PINMUX_GPIO169__FUNC_B1_MSDC2_CMD>,
				<PINMUX_GPIO171__FUNC_B1_MSDC2_DAT0>,
				<PINMUX_GPIO172__FUNC_B1_MSDC2_DAT1>,
				<PINMUX_GPIO173__FUNC_B1_MSDC2_DAT2>,
				<PINMUX_GPIO174__FUNC_B1_MSDC2_DAT3>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};

	mmc2_pins_eint: dat1-eint {
		pins-dat1 {
			pinmux = <PINMUX_GPIO172__FUNC_B_GPIO172>;
			input-enable;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};
	
	mmc2_pins_dat1: sdio-dat1 {
		pins-dat1 {
			pinmux = <PINMUX_GPIO172__FUNC_B1_MSDC2_DAT1>;
			input-enable;
			drive-strength = <MTK_DRIVE_6mA>;
			bias-pull-up = <MTK_PUPD_SET_R1R0_01>;
		};
	};
	
	spi0_pins: spi0-pins {
		pins-spi {
			pinmux = <PINMUX_GPIO69__FUNC_O_SPIM0_CSB>,
			<PINMUX_GPIO70__FUNC_O_SPIM0_CLK>,
			<PINMUX_GPIO71__FUNC_B0_SPIM0_MOSI>,
			<PINMUX_GPIO72__FUNC_B0_SPIM0_MISO>;
			bias-disable;
		};
	};
	
	spi1_pins: spi1-pins {
		pins-spi {
			pinmux = <PINMUX_GPIO75__FUNC_O_SPIM1_CSB>,
			<PINMUX_GPIO76__FUNC_O_SPIM1_CLK>,
			<PINMUX_GPIO77__FUNC_B0_SPIM1_MOSI>,
			<PINMUX_GPIO78__FUNC_B0_SPIM1_MISO>;
			bias-disable;
		};
	};
	
	pcie_pins_default: pcie-default {
		mux {
			pinmux = <PINMUX_GPIO47__FUNC_I1_WAKEN>,
			<PINMUX_GPIO48__FUNC_O_PERSTN>,
			<PINMUX_GPIO49__FUNC_B1_CLKREQN>;
			bias-pull-up;
		};
	};
	
	usb_default: usbdefault {
		iddig-pin {
			pinmux = <PINMUX_GPIO83__FUNC_B_GPIO83>;
			input-enable;
			bias-pull-up;
		};
		
		vbus-pins {
			pinmux = <PINMUX_GPIO84__FUNC_O_USB_DRVVBUS>;
			output-high;
		};
		
		valid-pin {
			pinmux = <PINMUX_GPIO85__FUNC_I0_VBUSVALID>;
			input-enable;
		};
	};
	
	usb1_default: usbp1default {
		valid_pin {
			pinmux = <PINMUX_GPIO88__FUNC_I0_VBUSVALID_1P>;
			input-enable;
		};
		
		usb-hub-3v3-en-pin {
			pinmux = <PINMUX_GPIO112__FUNC_B_GPIO112>;
			output-high;
		};
	};
	
	tpm_default: tpmdefault {
		pins-enable {
			pinmux = <PINMUX_GPIO6__FUNC_B_GPIO6>;
			output-high;
		};
	};

	pinctrl_stm32: stm32irq {
		irq-pin {
			pinmux = <PINMUX_GPIO13__FUNC_B_GPIO13>;
		};
	};

	disp_pwm0_pin_default: disp-pwm0-pin-default {
		pins0 {
			pinmux = <PINMUX_GPIO29__FUNC_O_DISP_PWM0>;
		};
	};
};


&i2c0 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c0_pin>;
	clock-frequency = <400000>;
	status = "okay";
};

&i2c1 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c1_pin>;
	clock-frequency = <400000>;
	status = "okay";

	// LVDS Bridge SN65DSI84
	lvds_bridge: lvds-bridge@2c {
		compatible = "ti,sn65dsi84";
		reg = <0x2c>;
		enable-gpios = <LVDS_EN GPIO_ACTIVE_HIGH>;
		status = "disabled";

		ports {
			#address-cells = <1>;
			#size-cells = <0>;

			port@0 {
				reg = <0>;

				lvds_bridge_in: endpoint {
					remote-endpoint =<&dsi0_out>;
					data-lanes = <1 2 3 4>;
				};
			};

			port@2 {
				reg = <2>;

				lvds_bridge_out_ch0: endpoint {
					remote-endpoint =<&panel0_in_ch0>;
				};
			};

		};
	};

	// RTC RV-3028
	rtc1: rtc@52 {
		compatible = "microcrystal,rv3028";
		reg = <0x52>;
		/* this makes sure the backup voltage of the RTC is enabled */
		backup-switch-mode = "direct";
		status = "okay";
	};

	// EEPROM 24AA025TI/MC
	eeprom: mc24aa024@50 {
		compatible = "atmel,24c02";
		reg = <0x50>;
		pagesize = <16>;
		vcc-supply = <&reg_1v8_run>;
		status = "okay";
	};
};

&i2c2 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c2_pin>;
	clock-frequency = <400000>;
	status = "okay";
	
	// SL B 9673 TPM2.0 @0x2E
	tpm: slb9673@2e {
		pinctrl-names = "default";
		pinctrl-0 = <&tpm_default>;
		compatible = "infineon,tpm_i2c_infineon";
		reg = <0x2e>;
		status = "okay";
	};
};

&i2c3 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c3_pin>;
	clock-frequency = <100000>;
	status = "okay";
	
	
	stm32: stm32@40 {
		reg = <0x40>;
		compatible = "seco,stm32";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_stm32>;
		interrupt-parent = <&pio>;
		interrupts = <EC_IRQ_OD_PIN IRQ_TYPE_EDGE_FALLING>;
		gpio-controller;
		#gpio-cells = <2>;
		ngpios =/bits/ 16  <STM32_NGPIOS>;
		interrupt-controller;
		#interrupt-cells = <2>;
		cpu-irq = <SMARC_CPU_IRQ_PIN>;
		#pwm-cells = <2>;
		npwm = <0>;
		status = "okay";

		gpio-line-names =
		"GPIO4", "GPIO5", "GPIO11", "GPIO12", "GPIO13", 
		"RTC_INT#", "SMB_ALERT#", "USB0_EN_OC#",
		"USB1_EN_OC#", "USB2_EN_OC#", "CPU_IRQ#_3V3",
		"USB_HUB_RST#", "WIFI_PWRDWN", "BATLOW#",
		"CHARGING#", "CHARGER_PRSNT#", "RESET_OUT#_3V3";

		gpio12 {
			gpio-hog;
			gpios = <USB_HUB_RST_PIN (GPIO_PUSH_PULL | GPIO_ACTIVE_LOW)>;
			line-name = "USB_HUB_RES#";
			output-low;
		};
	};
};

&i2c4 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c4_pin>;
	clock-frequency = <400000>;
	status = "okay";
};

&i2c5 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c5_pin>;
	clock-frequency = <400000>;
	status = "okay";
};

&i2c6 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c6_pin>;
	clock-frequency = <400000>;
	status = "okay";
};

&pmic {
	interrupt-parent = <&pio>;
	interrupts = <222 IRQ_TYPE_LEVEL_HIGH>;

	pmic_key: pmic-key {
		compatible = "mediatek,mt6359-keys";
		mediatek,long-press-mode = <1>;
		power-off-time-sec = <0>;
		
		power-key{
			linux,keycodes = <116>;
			wakeup-source;
		};
		
		home-key{
			linux,keycodes = <114>;
		};
	};
};

// System Companion Processor
&scp {
	memory-region = <&scp_mem_reserved>;
	status = "okay";
};

&keypad {
	mediatek,debounce-us = <32000>;
	mediatek,double-keys;
	keypad,num-rows = <1>;
	keypad,num-columns = <2>;
	linux,keymap =
	<MATRIX_KEY(0x00, 0x00, KEY_VOLUMEDOWN)
	MATRIX_KEY(0x00, 0x01, KEY_VOLUMEUP)>;
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&kpd_gpios_def_cfg>;
};


&uart0 {
	pinctrl-0 = <&uart0_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&uart1 {
	pinctrl-0 = <&uart1_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&uart2 {
	pinctrl-0 = <&uart2_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&uart3 {
	pinctrl-0 = <&uart3_pins>;
	pinctrl-names = "default";
	status = "okay";
};

&mipi_tx0 {
	status = "okay";
};

&mmc0 {
	status = "okay";
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc0_pins_default>;
	pinctrl-1 = <&mmc0_pins_uhs>;
	bus-width = <8>;
	max-frequency = <200000000>;
	cap-mmc-highspeed;
	mmc-hs200-1_8v;
	mmc-hs400-1_8v;
	supports-cqe;
	cap-mmc-hw-reset;
	no-sdio;
	no-sd;
	hs400-ds-delay = <0x1481b>;
	vmmc-supply = <&reg_vemc_pmu>;
	vqmmc-supply = <&reg_vufs18_pmu>;
	non-removable;
};

&mmc1 {
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc1_pins_default>;
	pinctrl-1 = <&mmc1_pins_uhs>;
	bus-width = <4>;
	max-frequency = <200000000>;
	cap-sd-highspeed;
	sd-uhs-sdr50;
	sd-uhs-sdr104;
	no-mmc;
	no-sdio;
	cd-gpios = <SDIO_CD GPIO_ACTIVE_LOW>;
	vqmmc-supply = <&reg_vsim1_pmu>;
};

#if 0
&mt6359_vpa_buck_reg {
	regulator-max-microvolt = <3100000>;
};

&mt6359_vsim1_ldo_reg {
	regulator-enable-ramp-delay = <480>;
};
#endif

&wifi_pwrseq {
	reset-gpios = <WIFI_PWRDWN GPIO_ACTIVE_LOW>;
	status = "okay";
};

&mmc2 {
	pinctrl-names = "default", "state_uhs";
	pinctrl-0 = <&mmc2_pins_default>;
	pinctrl-1 = <&mmc2_pins_uhs>;
	bus-width = <4>;
	max-frequency = <200000000>;
	cap-sd-highspeed;
	sd-uhs-sdr50;
	sd-uhs-sdr104;
	non-removable;
	vmmc-supply = <&reg_3v3_wifi>;
	vqmmc-supply = <&reg_1v8_run>;
	mmc-pwrseq = <&wifi_pwrseq>;
	no-sd;
	no-mmc;
	keep-power-in-suspend;
	status = "okay";
};

&mt6359_vpa_buck_reg {
	regulator-min-microvolt = <3300000>;
	regulator-max-microvolt = <3300000>;
	regulator-always-on;
};

&spi0 {
	pinctrl-0 = <&spi0_pins>;
	pinctrl-names = "default";
	mediatek,pad-select = <0>;
	#address-cells = <1>;
	#size-cells = <0>;
	status = "disabled";
	cs-gpios = <CAN_CS GPIO_ACTIVE_LOW>;

	can0: can@0 {
		compatible = "microchip,mcp2518fd";
		reg = <0>;
		clocks = <&can_clk>;
		spi-max-frequency = <5000000>;
		interrupts-extended = <CAN_INT IRQ_TYPE_LEVEL_LOW>;
		vdd-supply = <&reg_3v3_run>;
		xceiver-supply = <&reg_3v3_run>;
	};
};

&spi1 {
	pinctrl-0 = <&spi1_pins>;
	pinctrl-names = "default";
	mediatek,pad-select = <0>;
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	spidev@0 {
		compatible = "mediatek,aiot-board";
		spi-max-frequency = <5000000>;
		reg = <0>;
	};
};

// System Power Management Interface
&spmi {
	mt6315_6: mt6315@6 {
		compatible = "mediatek,mt6315-regulator";
		reg = <0x6 0 0xb 1>;
		
		regulators {
			mt6315_6_vbuck1: vbuck1 {
				regulator-compatible = "vbuck1";
				regulator-name = "Vbcpu";
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1193750>;
				regulator-enable-ramp-delay = <256>;
				regulator-allowed-modes = <0 1 2>;
				regulator-always-on;
				mtk,combined-regulator = <2>;
			};
			mt6315_6_vbuck3: vbuck3 {
				regulator-compatible = "vbuck3";
				regulator-name = "Vdd2";
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1193750>;
				regulator-enable-ramp-delay = <256>;
				regulator-allowed-modes = <0 1 2>;
				regulator-always-on;
			};
			mt6315_6_vbuck4: vbuck4 {
				regulator-compatible = "vbuck4";
				regulator-name = "Vddq";
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1193750>;
				regulator-enable-ramp-delay = <256>;
				regulator-allowed-modes = <0 1 2>;
				regulator-always-on;
			};
		};
	};
};

&pcie {
	pinctrl-names = "default";
	pinctrl-0 = <&pcie_pins_default>;
	status = "okay";
};

&pciephy {
	status = "okay";
};


// USB 2.0 P0 (USB0)
// CN30 on BB (OTG)
&ssusb {
	pinctrl-0 = <&usb_default>;
	pinctrl-names = "default";
	maximum-speed = "high-speed";
	usb-role-switch;
	dr_mode = "otg";
	vusb33-supply = <&reg_vusb_pmu>;
	wakeup-source;
};

&u2port0 {
	status = "okay";
};

&u3phy0 {
	status = "okay";
};

// USB 3.0 P1 (USB2)
// CN72 on BB
&ssusb1 {
	pinctrl-0 = <&usb1_default>;
	pinctrl-names = "default";
	vusb33-supply = <&reg_vusb_pmu>;
	dr_mode = "host";
	mediatek,usb3-drd;
	usb-role-switch;
	wakeup-source;
};

&u2port1 {
	status = "okay";
};

&u3port1 {
	status = "okay";
};

&u3phy1 {
	status = "okay";
};

// USB 2.0 P2 (USB Hub)
&ssusb2 {
	maximum-speed = "high-speed";
	dr_mode = "host";
	vusb33-supply = <&reg_3v3_run>;
};

&u2port2 {
	status = "okay";
};

&u3phy2 {
	status = "okay";
};

&mfg0 {
	domain-supply = <&mt6359_vproc2_buck_reg>;
};

&sound {
	compatible = "mediatek,mt8390-evk";
	model = "mt8390-evk";
	pinctrl-names = "default";
	pinctrl-0 = <&aud_pins_default>;
	status = "okay";

	dai-link-0 {
		sound-dai = <&afe>;
		dai-link-name = "ETDM3_OUT_BE";
		status = "okay";

		codec-0 {
			sound-dai = <&hdmi0>;
		};
	};

	dai-link-1 {
		sound-dai = <&afe>;
		dai-link-name = "DPTX_BE";
		status = "disabled";

		codec-0 {
			sound-dai = <&dp_tx>;
		};
	};
};

&cam_vcore {
	domain-supply = <&mt6359_vproc1_buck_reg>;
};

&img_vcore {
	domain-supply = <&mt6359_vproc1_buck_reg>;
};
