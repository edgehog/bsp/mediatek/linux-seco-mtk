// SPDX-License-Identifier: GPL-2.0+
/*
 * Generic LVDS panel driver
 *
 * Copyright (C) 2016 Laurent Pinchart
 * Copyright (C) 2016 Renesas Electronics Corporation
 *
 * Contact: Laurent Pinchart (laurent.pinchart@ideasonboard.com)
 */

#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/regulator/consumer.h>
#include <linux/slab.h>

#include <video/display_timing.h>
#include <video/of_display_timing.h>
#include <video/videomode.h>

#include <drm/drm_crtc.h>
#include <drm/drm_panel.h>
#include <drm/drm_mipi_dsi.h>

#define DUMP_TIMINGS 0

struct panel_lvds {
	struct drm_panel panel;
	struct device *dev;

	const char *label;
	unsigned int width;
	unsigned int height;
	struct videomode video_mode;
	unsigned int bus_format;
	bool data_mirror;

	struct regulator *supply;

	struct gpio_desc *enable_gpio;
	struct gpio_desc *reset_gpio;

	enum drm_panel_orientation orientation;
};

static inline struct panel_lvds *to_panel_lvds(struct drm_panel *panel)
{
	return container_of(panel, struct panel_lvds, panel);
}

static int panel_lvds_unprepare(struct drm_panel *panel)
{
	struct panel_lvds *lvds = to_panel_lvds(panel);

	if (lvds->enable_gpio)
		gpiod_set_value_cansleep(lvds->enable_gpio, 0);

	if (lvds->supply)
		regulator_disable(lvds->supply);

	return 0;
}

static int panel_lvds_prepare(struct drm_panel *panel)
{
	struct panel_lvds *lvds = to_panel_lvds(panel);

	if (lvds->supply) {
		int err;

		err = regulator_enable(lvds->supply);
		if (err < 0) {
			dev_err(lvds->dev, "failed to enable supply: %d\n",
				err);
			return err;
		}
	}

	if (lvds->enable_gpio)
		gpiod_set_value_cansleep(lvds->enable_gpio, 1);

	return 0;
}

static int panel_lvds_get_modes(struct drm_panel *panel,
				struct drm_connector *connector)
{
	struct panel_lvds *lvds = to_panel_lvds(panel);
	struct drm_display_mode *mode;

	mode = drm_mode_create(connector->dev);
	if (!mode)
		return 0;

	drm_display_mode_from_videomode(&lvds->video_mode, mode);
	mode->type |= DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED;
#if DUMP_TIMINGS
	dev_info(lvds->dev, "%s: clock=%d\n", __FUNCTION__, mode->clock);
	dev_info(lvds->dev, "%s: hdisplay=%d\n", __FUNCTION__, mode->hdisplay);
	dev_info(lvds->dev, "%s: hsync_start=%d\n", __FUNCTION__, mode->hsync_start);
	dev_info(lvds->dev, "%s: hsync_end=%d\n", __FUNCTION__, mode->hsync_end);
	dev_info(lvds->dev, "%s: htotal=%d\n", __FUNCTION__, mode->htotal);
	dev_info(lvds->dev, "%s: vdisplay=%d\n", __FUNCTION__, mode->vdisplay);
	dev_info(lvds->dev, "%s: vsync_start=%d\n", __FUNCTION__, mode->vsync_start);
	dev_info(lvds->dev, "%s: vsync_end=%d\n", __FUNCTION__, mode->vsync_end);
	dev_info(lvds->dev, "%s: vtotal=%d\n", __FUNCTION__, mode->vtotal);
#endif
	drm_mode_probed_add(connector, mode);

	connector->display_info.width_mm = lvds->width;
	connector->display_info.height_mm = lvds->height;
	switch(lvds->bus_format)
	{
	case MEDIA_BUS_FMT_RGB666_1X7X3_SPWG:
		connector->display_info.bpc = 6;
		break;
	default:
		connector->display_info.bpc = 8;
	}
	drm_display_info_set_bus_formats(&connector->display_info,
					 &lvds->bus_format, 1);
	drm_bus_flags_from_videomode(&lvds->video_mode,
								 &connector->display_info.bus_flags);
	connector->display_info.bus_flags |= lvds->data_mirror
					  ? DRM_BUS_FLAG_DATA_LSB_TO_MSB
					  : DRM_BUS_FLAG_DATA_MSB_TO_LSB;
	drm_connector_set_panel_orientation(connector, lvds->orientation);

	return 1;
}

static const struct drm_panel_funcs panel_lvds_funcs = {
	.unprepare = panel_lvds_unprepare,
	.prepare = panel_lvds_prepare,
	.get_modes = panel_lvds_get_modes,
};

static int panel_lvds_parse_dt(struct panel_lvds *lvds)
{
	struct device_node *np = lvds->dev->of_node;
	struct display_timing timing;
	const char *mapping;
	int ret;

	ret = of_drm_get_panel_orientation(np, &lvds->orientation);
	if (ret < 0) {
		dev_err(lvds->dev, "%pOF: failed to get orientation %d\n", np, ret);
		return ret;
	}

	ret = of_get_display_timing(np, "panel-timing", &timing);
	if (ret < 0) {
		dev_err(lvds->dev, "%pOF: problems parsing panel-timing (%d)\n",
			np, ret);
		return ret;
	}

	videomode_from_timing(&timing, &lvds->video_mode);

	ret = of_property_read_u32(np, "width-mm", &lvds->width);
	if (ret < 0) {
		dev_err(lvds->dev, "%pOF: invalid or missing %s DT property\n",
			np, "width-mm");
		return -ENODEV;
	}
	ret = of_property_read_u32(np, "height-mm", &lvds->height);
	if (ret < 0) {
		dev_err(lvds->dev, "%pOF: invalid or missing %s DT property\n",
			np, "height-mm");
		return -ENODEV;
	}

	of_property_read_string(np, "label", &lvds->label);

	ret = of_property_read_string(np, "data-mapping", &mapping);
	if (ret < 0) {
		dev_err(lvds->dev, "%pOF: invalid or missing %s DT property\n",
			np, "data-mapping");
		return -ENODEV;
	}

	if (!strcmp(mapping, "jeida-18")) {
		lvds->bus_format = MEDIA_BUS_FMT_RGB666_1X7X3_SPWG;
	} else if (!strcmp(mapping, "jeida-24")) {
		lvds->bus_format = MEDIA_BUS_FMT_RGB888_1X7X4_JEIDA;
	} else if (!strcmp(mapping, "vesa-24")) {
		lvds->bus_format = MEDIA_BUS_FMT_RGB888_1X7X4_SPWG;
	} else if (!strcmp(mapping, "rgb888-1x24")) {
		lvds->bus_format = MEDIA_BUS_FMT_RGB888_1X24;
	} else {
		dev_err(lvds->dev, "%pOF: invalid or missing %s DT property\n",
			np, "data-mapping");
		return -EINVAL;
	}

	lvds->data_mirror = of_property_read_bool(np, "data-mirror");

	return 0;
}

static int panel_lvds_probe(struct platform_device *pdev)
{
	const int *connector;
	struct panel_lvds *lvds;
	int ret;

	connector = of_device_get_match_data(&pdev->dev);
	if(!connector)
		return -ENODEV;
	lvds = devm_kzalloc(&pdev->dev, sizeof(*lvds), GFP_KERNEL);
	if (!lvds)
		return -ENOMEM;

	lvds->dev = &pdev->dev;

	ret = panel_lvds_parse_dt(lvds);
	if (ret < 0)
		return ret;

	lvds->supply = devm_regulator_get_optional(lvds->dev, "power");
	if (IS_ERR(lvds->supply)) {
		ret = PTR_ERR(lvds->supply);

		if (ret != -ENODEV) {
			if (ret != -EPROBE_DEFER)
				dev_err(lvds->dev, "failed to request regulator: %d\n",
					ret);
			return ret;
		}

		lvds->supply = NULL;
	}

	/* Get GPIOs and backlight controller. */
	lvds->enable_gpio = devm_gpiod_get_optional(lvds->dev, "enable",
						     GPIOD_OUT_LOW);
	if (IS_ERR(lvds->enable_gpio)) {
		ret = PTR_ERR(lvds->enable_gpio);
		dev_err(lvds->dev, "failed to request %s GPIO: %d\n",
			"enable", ret);
		return ret;
	}

	lvds->reset_gpio = devm_gpiod_get_optional(lvds->dev, "reset",
						     GPIOD_OUT_HIGH);
	if (IS_ERR(lvds->reset_gpio)) {
		ret = PTR_ERR(lvds->reset_gpio);
		dev_err(lvds->dev, "failed to request %s GPIO: %d\n",
			"reset", ret);
		return ret;
	}

	/*
	 * TODO: Handle all power supplies specified in the DT node in a generic
	 * way for panels that don't care about power supply ordering. LVDS
	 * panels that require a specific power sequence will need a dedicated
	 * driver.
	 */

	/* Register the panel. */
	drm_panel_init(&lvds->panel, lvds->dev, &panel_lvds_funcs,
		       *connector);

	ret = drm_panel_of_backlight(&lvds->panel);
	if (ret)
		return ret;

	drm_panel_add(&lvds->panel);

	dev_set_drvdata(lvds->dev, lvds);
	return 0;
}

static int panel_lvds_remove(struct platform_device *pdev)
{
	struct panel_lvds *lvds = platform_get_drvdata(pdev);

	drm_panel_remove(&lvds->panel);

	drm_panel_disable(&lvds->panel);

	return 0;
}

static int connector_lvds = DRM_MODE_CONNECTOR_LVDS;
static int connector_edp = DRM_MODE_CONNECTOR_eDP;
static int connector_dsi = DRM_MODE_CONNECTOR_DSI;

static const struct of_device_id panel_lvds_of_table[] = {
	{ .compatible = "seco,panel-lvds", .data = &connector_lvds},
	{ .compatible = "seco,panel-edp", .data = &connector_edp},
	{ .compatible = "seco,panel-dsi", .data = &connector_dsi},
	{ /* Sentinel */ },
};

MODULE_DEVICE_TABLE(of, panel_lvds_of_table);

static struct platform_driver panel_lvds_driver = {
	.probe		= panel_lvds_probe,
	.remove		= panel_lvds_remove,
	.driver		= {
		.name	= "panel-lvds",
		.of_match_table = panel_lvds_of_table,
	},
};

//module_platform_driver(panel_lvds_driver);

static int panel_dsi_probe(struct mipi_dsi_device *pdev)
{
	const int *connector;
	struct panel_lvds *lvds;
	int ret;

	dev_info(&pdev->dev, "%s\n", __FUNCTION__);
	connector = of_device_get_match_data(&pdev->dev);
	if(!connector)
		return -ENODEV;
	lvds = devm_kzalloc(&pdev->dev, sizeof(*lvds), GFP_KERNEL);
	if (!lvds)
		return -ENOMEM;

	lvds->dev = &pdev->dev;

	ret = panel_lvds_parse_dt(lvds);
	if (ret < 0)
		return ret;

	lvds->supply = devm_regulator_get_optional(lvds->dev, "power");
	if (IS_ERR(lvds->supply)) {
		ret = PTR_ERR(lvds->supply);

		if (ret != -ENODEV) {
			if (ret != -EPROBE_DEFER)
				dev_err(lvds->dev, "failed to request regulator: %d\n",
					ret);
			return ret;
		}

		lvds->supply = NULL;
	}

	/* Get GPIOs and backlight controller. */
	lvds->enable_gpio = devm_gpiod_get_optional(lvds->dev, "enable",
						     GPIOD_OUT_LOW);
	if (IS_ERR(lvds->enable_gpio)) {
		ret = PTR_ERR(lvds->enable_gpio);
		dev_err(lvds->dev, "failed to request %s GPIO: %d\n",
			"enable", ret);
		return ret;
	}

	lvds->reset_gpio = devm_gpiod_get_optional(lvds->dev, "reset",
						     GPIOD_OUT_HIGH);
	if (IS_ERR(lvds->reset_gpio)) {
		ret = PTR_ERR(lvds->reset_gpio);
		dev_err(lvds->dev, "failed to request %s GPIO: %d\n",
			"reset", ret);
		return ret;
	}

	/*
	 * TODO: Handle all power supplies specified in the DT node in a generic
	 * way for panels that don't care about power supply ordering. LVDS
	 * panels that require a specific power sequence will need a dedicated
	 * driver.
	 */

	/* Register the panel. */
	//dev_info(&pdev->dev, "%s: init panel\n", __FUNCTION__);
	drm_panel_init(&lvds->panel, lvds->dev, &panel_lvds_funcs,
		       *connector);

	ret = drm_panel_of_backlight(&lvds->panel);
	if (ret)
		return ret;

	//dev_info(&pdev->dev, "%s: add panel\n", __FUNCTION__);
	drm_panel_add(&lvds->panel);

	mipi_dsi_set_drvdata(pdev, lvds);

	// TODO: read the dsi parameters from device tree
	pdev->lanes=4;
	pdev->format=MIPI_DSI_FMT_RGB888;
	pdev->mode_flags = MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_BURST;

	//dev_info(&pdev->dev, "%s: attach panel\n", __FUNCTION__);
	ret = mipi_dsi_attach(pdev);
	if(ret)
	{
		//dev_info(&pdev->dev, "%s: remove panel\n", __FUNCTION__);
		drm_panel_remove(&lvds->panel);
	}

	dev_info(&pdev->dev, "%s: ret=%d\n", __FUNCTION__,ret);

	return ret;
}

static int panel_dsi_remove(struct mipi_dsi_device *pdev)
{
	struct panel_lvds *lvds = mipi_dsi_get_drvdata(pdev);

	dev_info(&pdev->dev, "%s\n", __FUNCTION__);
	drm_panel_remove(&lvds->panel);

	drm_panel_disable(&lvds->panel);

	return 0;
}

static struct mipi_dsi_driver panel_dsi_driver = {
	.probe		= panel_dsi_probe,
	.remove		= panel_dsi_remove,
	.driver		= {
		.name	= "panel-dsi",
		.of_match_table = panel_lvds_of_table,
	},
};

//module_mipi_dsi_driver(panel_dsi_driver);

static int __init panel_lvds_init(void)
{
	int err;
	err=platform_driver_register(&panel_lvds_driver);
	if(!err)
	{
		err=mipi_dsi_driver_register(&panel_dsi_driver);
		if(err)
			platform_driver_unregister(&panel_lvds_driver);
	}
	return err;
}
module_init(panel_lvds_init);

static void __exit panel_lvds_exit(void)
{
	mipi_dsi_driver_unregister(&panel_dsi_driver);
	platform_driver_unregister(&panel_lvds_driver);
}
module_exit(panel_lvds_exit);

MODULE_AUTHOR("Laurent Pinchart <laurent.pinchart@ideasonboard.com>");
MODULE_DESCRIPTION("LVDS Panel Driver");
MODULE_LICENSE("GPL");
