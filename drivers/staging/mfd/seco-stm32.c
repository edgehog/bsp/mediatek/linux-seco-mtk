/*****************************************************************************/
/* SPDX-License-Identifier: (GPL-2.0+ OR MIT)                                */
/*
 *
 * STM32-based GPIO-extenter and Pulse-Width-Modulator by SECO NE
 * Also reboots/halts the system
 *
 * Copyright 2024 SECO NE    Dietmar Muscholik <dietmar.muscholik@seco.com>
 *
 *****************************************************************************/

#include <linux/i2c.h>
#include <linux/pwm.h>
#include <linux/gpio/driver.h>
#include <linux/reboot.h>
#include <linux/limits.h>

#define DRIVER_NAME "seco-stm32"

// verbosity
// 0:   only errors and warnings are reported
// 1:   report most function calls
// 2:   report all function calls but interrupts
// > 2: even interrupts and i2c-messages are printed
// don't forget to set it to 0 when you're finished with testing!!!
#define STM32_DEBUG 0

// i2c
#define STM32_READ 0
#define STM32_WRITE 1
#define STM32_I2C_MSG_MAX 8

// registers
enum stm32_regs {
	STM32_REG_ID = 0x01,
	STM32_REG_CONTROL, // reboot, halt
	STM32_REG_CONFIG,
	STM32_REG_PIN_CONFIG,
	STM32_REG_PIN_SET,
	STM32_REG_PIN_GET,
	STM32_REG_PWM_CONFIG, // 0-100
	STM32_REG_ALTERNATE_FUNCTION,
	STM32_REG_ADC_READ,
	STM32_REG_INT_STATUS0,
	STM32_REG_INT_ENABLE0,
	STM32_REG_INT_STATUS1,
	STM32_REG_INT_ENABLE1,
	STM32_REG_INT_STATUS2,
	STM32_REG_INT_ENABLE2,
	STM32_REG_SHUTDOWN_DELAY = 0x10,
	STM32_REG_SECO_CODE = 0x80,
	STM32_REG_RAM_CODE = 0x90,
	STM32_REG_VERSION = 0xf0, // firmware version
};

// pin controls
#define STM32_PINCFG_IN_NP 0x01 // input no pull
#define STM32_PINCFG_IN_PD 0x10 // input pull down
#define STM32_PINCFG_IN_PU 0x11 // input pull up
#define STM32_PINCFG_OUT_PP_LO 0x20 // output push/pull low
#define STM32_PINCFG_OUT_PP_HI 0x21 // output push/pull high
#define STM32_PINCFG_OUT_OD_LO 0x40 // output open drain low
#define STM32_PINCFG_OUT_OD_HI 0x41 // output open drain high
#define STM32_PINCFG_IRQ_EDGE_RISING 0x80
#define STM32_PINCFG_IRQ_EDGE_FALLING 0x81
#define STM32_PINCFG_IRQ_EDGE_BOTH 0x82

#define STM32_REG_INT_COUNT 3
#define STM32_INT_MAX (STM32_REG_INT_COUNT * BITS_PER_BYTE)


struct gpio_pin {
	int dir;
	// A dir<0 means error,
	// currently -EBUSY is used if a pin is used as pwm.
	int config;
	unsigned int irq;
};

typedef union {
	uint32_t mask32;
	uint8_t mask8[STM32_REG_INT_COUNT];
} irq_mask;

struct pwm_pin {
	int no;
	struct pwm_state state;
};

enum boot_mode {
	STM32_BOOT_OFF = -1,
	STM32_BOOT_INTERNAL = 0,
	STM32_BOOT_SERIAL,
	STM32_BOOT_USDHC1,
	STM32_BOOT_USDHC2,
	STM32_BOOT_POWER_OFF,
	STM32_BOOT_BOOT_SEL,
};
#define STM32_BOOT_DEFAULT STM32_BOOT_BOOT_SEL

#ifndef SYS_OFF_PRIO_DEFAULT
#define SYS_OFF_PRIO_DEFAULT 0
#endif

struct stm32 {
	unsigned char chip_id, fw_ver, seco_code;
	struct gpio_chip gpiochip;
	struct gpio_pin *gpiopins;
	irq_mask irqmask;
	int irq_changed;
	struct mutex irq_lock;
	int cpu_irq;
	struct pwm_chip pwmchip;
	struct pwm_pin *pwmpins;
	struct notifier_block reboot;
	enum boot_mode bootmode;
	int reboot_delay; // reboot/halt-delay in 10ms steps
};
/*
  It does not make much sense to store the device, i2c_client, irq and all
  that stuff in the stm32 structure because everything we need is already
  present in the substructures. We only need to know how to get it.

  How to get...
	struct stm32*
		dev_get_drvdata(struct device *)
		gpiochip_get_data(struct gpio_chip *)
		(struct stm32*)(void *data) // interrupt handler
	struct i2c_client*
	    to_i2c_client(struct device *)
	struct device*
	    struct i2c_client client.dev
	    struct pwm_chip chip.dev
		struct gpio_chip chip.parent
		struct stm32 stm32.pwmchip.dev
		struct stm32 stm32.gpiochip.parent
	struct gpio_chip *
		irq_data_get_irq_chip_data(struct irq_data *)
*/


// chip-id
static int stm32_id[] = {
	0xa5, // MAURY
	0x96  // WILK
};

/*
 general purpose i2c transfer

 parameters:
   client: where to write to
   dir:    either STM32_READ or STM32_WRITE
   reg:    register to write to/read from
   argc:   number of additional parameters of type int

 returns:
   on successfull read the positive register value will be returned
   on successfull write 0 will be returned
   on failure -errno will be returned

 examples:
   read register reg
   val = i2c_xfer(client, STM32_READ, reg, 0);

   write val to register reg
   err = i2c_xfer(client, STM32_WRITE, reg, 1, val);
*/
static int i2c_xfer(struct i2c_client *client, int dir, int reg,
					int argc, ...)
{
	int err;
	u8 wbuf[STM32_I2C_MSG_MAX];
	u8 rbuf;
	size_t buf_len=argc+1;
	struct i2c_msg msg[] = {
		{client->addr, 0, buf_len, wbuf},
		{client->addr, I2C_M_RD, 1, &rbuf},
	};
	va_list args;
	size_t n;
#if STM32_DEBUG > 2
	char buffer[256];
#endif

	va_start(args,argc);
	wbuf[0]=(u8)reg;
	for(n=1; n<buf_len && n<STM32_I2C_MSG_MAX; n++)
		wbuf[n]=(u8)va_arg(args,int);
	va_end(args);

#if STM32_DEBUG > 2
	sprintf(buffer, "%s, sending: ", __FUNCTION__);
	for(n=0; n<buf_len; n++)
		sprintf(buffer+strlen(buffer), " 0x%02x", (int)wbuf[n]);
	dev_info(&client->dev, buffer);
#endif
	err=i2c_transfer(client->adapter, msg, dir==STM32_READ ? 2 : 1);
	if(err < 0)
	{
		dev_err(&client->dev, "%s: err=%d\n", __FUNCTION__, err);
		return err;
	}

#if STM32_DEBUG > 2
	dev_info(&client->dev, "%s: err=%d\n", __FUNCTION__, err);
	if(dir==STM32_READ)
		dev_info(&client->dev, "%s, received: 0x%02x\n", __FUNCTION__, (int)rbuf);
#endif
	if(dir == STM32_READ)
		return (int)rbuf;

	return 0;
}


// sysfs
// can be found in /sys/bus/i2c/devices/b-aaaa
// were b is the bus number and aaaa is the address in hex

// firmware version
static ssize_t fw_ver_show(struct device *dev,
						   struct device_attribute *attr, char *buf)
{
	struct stm32 *stm32=dev_get_drvdata(dev);
	return sprintf(buf, "%d\n", (int)stm32->fw_ver);
}
static DEVICE_ATTR_RO(fw_ver);

// chip id
static ssize_t chip_id_show(struct device *dev,
							struct device_attribute *attr, char *buf)
{
	struct stm32 *stm32=dev_get_drvdata(dev);
	return sprintf(buf, "%d\n", (int)stm32->chip_id);
}
static DEVICE_ATTR_RO(chip_id);

// seco-code
static ssize_t seco_code_show(struct device *dev,
							struct device_attribute *attr, char *buf)
{
	struct stm32 *stm32=dev_get_drvdata(dev);
	return sprintf(buf, "%d\n", (int)stm32->seco_code);
}
static DEVICE_ATTR_RO(seco_code);

// boot mode
static ssize_t boot_mode_show(struct device *dev,
							  struct device_attribute *attr, char *buf)
{
	struct stm32 *stm32=dev_get_drvdata(dev);
	return sprintf(buf, "%d\n", (int)stm32->bootmode);
}

static ssize_t boot_mode_store(struct device *dev,
							   struct device_attribute *attr,
							   const char *buf, size_t count)
{
	struct stm32 *stm32=dev_get_drvdata(dev);
	int mode;

	if(sscanf(buf, "%d", &mode) != 1)
		return -EINVAL;

	if(mode < STM32_BOOT_OFF || mode > STM32_BOOT_BOOT_SEL)
		return -EINVAL;

	stm32->bootmode=mode;

	return count;
}
static DEVICE_ATTR_RW(boot_mode);


// gpio stuff
static int stm32_gpio_request(struct gpio_chip *gc,
							  unsigned int offset)
{
	struct stm32 *stm32=(struct stm32 *)dev_get_drvdata(gc->parent);

#if STM32_DEBUG > 1
	dev_info(gc->parent, "%s: offset=%d\n",
			 __FUNCTION__, offset);
#endif
	if(stm32->gpiopins[offset].dir < 0)
		return stm32->gpiopins[offset].dir;
	return 0;
}

static int stm32_gpio_get_dir(struct gpio_chip *gc,
							  unsigned int offset)
{
	struct stm32 *stm32=(struct stm32 *)dev_get_drvdata(gc->parent);

#if STM32_DEBUG > 1
	dev_info(gc->parent, "%s: offset=%d, dir=%d\n",
			 __FUNCTION__, offset, stm32->gpiopins[offset].dir);
#endif
	return stm32->gpiopins[offset].dir;
}

static int stm32_gpio_dir_input(struct gpio_chip *gc,
								unsigned int offset)
{
	struct stm32 *stm32=(struct stm32 *)dev_get_drvdata(gc->parent);
	int err;

#if STM32_DEBUG
	dev_info(gc->parent, "%s: offset=%d\n",
			 __FUNCTION__, offset);
#endif
	err=i2c_xfer(to_i2c_client(gc->parent), STM32_WRITE,
				 STM32_REG_PIN_CONFIG, 2, offset,
				 STM32_PINCFG_IN_NP);
	if(!err)
		stm32->gpiopins[offset].dir=GPIO_LINE_DIRECTION_IN;
	return err;
}

static int stm32_gpio_dir_output(struct gpio_chip *gc,
								 unsigned int offset, int value)
{
	struct stm32 *stm32=(struct stm32 *)dev_get_drvdata(gc->parent);
	int err;

#if STM32_DEBUG
	dev_info(gc->parent, "%s: offset=%d, value=%d\n",
			 __FUNCTION__, offset, value);
#endif
	err=i2c_xfer(to_i2c_client(gc->parent), STM32_WRITE,
				 STM32_REG_PIN_CONFIG, 2, offset,
				 value ? stm32->gpiopins[offset].config | value :
				 stm32->gpiopins[offset].config & ~value);
	if(!err)
		stm32->gpiopins[offset].dir=GPIO_LINE_DIRECTION_OUT;
	return err;
}

static int stm32_gpio_get(struct gpio_chip *gc,
						  unsigned int offset)
{
#if STM32_DEBUG
	dev_info(gc->parent, "%s: offset=%d\n",
			 __FUNCTION__, offset);
#endif
	return i2c_xfer(to_i2c_client(gc->parent), STM32_READ,
					STM32_REG_PIN_GET, 1, offset);
}

static void stm32_gpio_set(struct gpio_chip *gc,
						   unsigned int offset, int value)
{
#if STM32_DEBUG
	dev_info(gc->parent, "%s: offset=%d, value=%d\n",
			 __FUNCTION__, offset, value);
#endif
	i2c_xfer(to_i2c_client(gc->parent), STM32_WRITE,
			 STM32_REG_PIN_SET, 2, offset, value);
}

static int stm32_gpio_set_config(struct gpio_chip *gc,
								 unsigned int offset,
								 unsigned long config)
{
	struct stm32 *stm32=(struct stm32 *)dev_get_drvdata(gc->parent);

#if STM32_DEBUG
	dev_info(gc->parent, "%s: offset=%d param=%d, argument=%d\n",
			 __FUNCTION__, offset,
			 pinconf_to_config_param(config),
			 pinconf_to_config_argument(config));
#endif
	switch(pinconf_to_config_param(config))
	{
	case PIN_CONFIG_DRIVE_OPEN_DRAIN:
		stm32->gpiopins[offset].config=STM32_PINCFG_OUT_OD_LO;
		break;
	case PIN_CONFIG_DRIVE_PUSH_PULL:
		stm32->gpiopins[offset].config=STM32_PINCFG_OUT_PP_LO;
		break;
	default:
		break;
	}

	return 0;
}


// irq stuff
static void stm32_irq_mask(struct irq_data *data)
{
	struct gpio_chip *chip=irq_data_get_irq_chip_data(data);
	struct stm32 *stm32=gpiochip_get_data(chip);

#if STM32_DEBUG
	dev_info(chip->parent, "%s: irq=%d, hwirq=%ld\n",
			 __FUNCTION__, data->irq, data->hwirq);
#endif
	stm32->gpiopins[data->hwirq].irq = 0;
	stm32->irqmask.mask32 &= ~(1 << data->hwirq);
	stm32->irq_changed=TRUE;
}

static void stm32_irq_unmask(struct irq_data *data)
{
	struct gpio_chip *chip=irq_data_get_irq_chip_data(data);
	struct stm32 *stm32=gpiochip_get_data(chip);

#if STM32_DEBUG
	dev_info(chip->parent, "%s: irq=%d, hwirq=%ld\n",
			 __FUNCTION__, data->irq, data->hwirq);
#endif
	stm32->gpiopins[data->hwirq].irq = data->irq;
	stm32->irqmask.mask32 |= (1 << data->hwirq);
	stm32->irq_changed=TRUE;
}

static int stm32_irq_set_type(struct irq_data *data,
							  unsigned int flow_type)
{
	// flow_type is defined in include/linux/irq.h
	struct gpio_chip *chip=irq_data_get_irq_chip_data(data);
	struct stm32 *stm32=gpiochip_get_data(chip);
	int type;

#if STM32_DEBUG
	dev_info(chip->parent, "%s: hwirq=%ld, flow_type=%d\n",
			 __FUNCTION__, data->hwirq, flow_type);
#endif
	switch(flow_type)
	{
	case IRQ_TYPE_EDGE_RISING:
		type=STM32_PINCFG_IRQ_EDGE_RISING;
		break;
	case IRQ_TYPE_EDGE_FALLING:
		type=STM32_PINCFG_IRQ_EDGE_FALLING;
		break;
	case IRQ_TYPE_EDGE_BOTH:
		type=STM32_PINCFG_IRQ_EDGE_BOTH;
		break;
	default:
		return -EINVAL;
	}

	stm32->gpiopins[data->hwirq].config = type;
	stm32->irq_changed=TRUE;

	return 0;
}

static void stm32_irq_bus_lock(struct irq_data *data)
{
	struct gpio_chip *chip=irq_data_get_irq_chip_data(data);
	struct stm32 *stm32=gpiochip_get_data(chip);

#if STM32_DEBUG
	dev_info(chip->parent, "%s: hwirq=%ld\n", __FUNCTION__, data->hwirq);
#endif
	mutex_lock(&stm32->irq_lock);
}

/*
  This actually writes the irq-configuration to the chip.
  Any attempt to do so in mask, unmask or set_type will cause
  "BUG: scheduling while atomic..."
*/
static void stm32_irq_bus_sync_unlock(struct irq_data *data)
{
	struct gpio_chip *chip=irq_data_get_irq_chip_data(data);
	struct stm32 *stm32=gpiochip_get_data(chip);
	struct i2c_client *client=to_i2c_client(chip->parent);
	irq_mask mask;
	int n,reg;

#if STM32_DEBUG
	dev_info(chip->parent, "%s: hwirq=%ld\n", __FUNCTION__, data->hwirq);
#endif
	if(stm32->irq_changed)
	{
		i2c_xfer(client, STM32_WRITE, STM32_REG_PIN_CONFIG, 2,
				 data->hwirq, stm32->gpiopins[data->hwirq].config);
		mask.mask32 = 1<<data->hwirq;
		for(n=0, reg=STM32_REG_INT_ENABLE0; n<STM32_REG_INT_COUNT; n++, reg+=2)
			if(mask.mask8[n])
				i2c_xfer(client, STM32_WRITE, reg,
						 1, stm32->irqmask.mask8[n]);
		stm32->irq_changed=FALSE;
	}
	mutex_unlock(&stm32->irq_lock);
}

static struct irq_chip irq_chip = {
	.name = DRIVER_NAME,
	.irq_mask = stm32_irq_mask,
	.irq_unmask = stm32_irq_unmask,
	.irq_set_type = stm32_irq_set_type,
	.irq_bus_lock = stm32_irq_bus_lock,
	.irq_bus_sync_unlock = stm32_irq_bus_sync_unlock,
	//.flags = IRQCHIP_IMMUTABLE,
};

// handle interrupts
static irqreturn_t stm32_irq_handler(int irq, void *data)
{
	struct stm32 *stm32=(struct stm32 *)data;
	struct device *dev=stm32->gpiochip.parent;
	struct i2c_client *client=to_i2c_client(dev);
	irq_mask status;
	int n,reg;
	int err;
	int bit;

#if STM32_DEBUG > 2
	dev_info(dev, "%s: irq=%d", __FUNCTION__, irq);
#endif
	status.mask32=0;
	for(n=0, reg=STM32_REG_INT_STATUS0; n<STM32_REG_INT_COUNT; n++, reg+=2)
	{
		err=i2c_xfer(client, STM32_READ, reg, 0);
		if(err < 0)
			return IRQ_RETVAL(TRUE);
		status.mask8[n]=(uint8_t)err;
	}
#if STM32_DEBUG > 2
	dev_info(dev, "%s: status=0x%08x", __FUNCTION__, status.mask32);
#endif
	for(n=0, bit=1; n<STM32_INT_MAX; n++, bit<<=1)
		if((status.mask32 & bit) && stm32->gpiopins[n].irq)
		{
#if STM32_DEBUG > 2
			dev_info(dev, "%s: nested_irq=%d",
					 __FUNCTION__, stm32->gpiopins[n].irq);
#endif
			handle_nested_irq(stm32->gpiopins[n].irq);
		}

	return IRQ_RETVAL(TRUE);
}

// pwm stuff
static int stm32_pwm_apply(struct pwm_chip *chip, struct pwm_device *pwm,
						   const struct pwm_state *state)
{
	struct stm32 *stm32=(struct stm32 *)dev_get_drvdata(chip->dev);
	struct i2c_client *i2c_client=to_i2c_client(chip->dev);
	int err;
	int perc;

	if(state->enabled)
		perc=100 * state->duty_cycle / state->period;
	else
		perc=0;
#if STM32_DEBUG
	dev_info(chip->dev, "%s: setting pwm%d to %d\n",
			 __FUNCTION__, pwm->hwpwm, perc);
#endif
	err=i2c_xfer(i2c_client, STM32_WRITE, STM32_REG_PWM_CONFIG,
				 2, stm32->pwmpins[pwm->hwpwm].no, perc);
	if(!err)
		memcpy(&stm32->pwmpins[pwm->hwpwm].state, state, sizeof(*state));

	return err;
}

static void stm32_pwm_get_state(struct pwm_chip *chip, struct pwm_device *pwm,
								struct pwm_state *state)
{
	struct stm32 *stm32=(struct stm32 *)dev_get_drvdata(chip->dev);

#if STM32_DEBUG
	dev_info(chip->dev,"%s\n",__FUNCTION__);
#endif
	memcpy(state, &stm32->pwmpins[pwm->hwpwm].state, sizeof(*state));
}

static const struct pwm_ops pwm_ops = {
	.apply = stm32_pwm_apply,
	.get_state = stm32_pwm_get_state,
	.owner = THIS_MODULE,
};


// reboot
static int stm32_reboot(struct notifier_block *nb,
						unsigned long action, void *data)
{
	struct stm32 *stm32=container_of(nb, struct stm32, reboot);
	struct device *dev=stm32->gpiochip.parent;
	enum boot_mode mode;

#if STM32_DEBUG
	dev_info(dev, "%s: action=%ld, data=0x%08lx\n",
			 __FUNCTION__, action, (long int)data);
#endif
	if(stm32->bootmode == STM32_BOOT_OFF)
		return NOTIFY_DONE;

	switch(action)
	{
	case SYS_RESTART:
		mode=stm32->bootmode;
		break;
	case SYS_HALT:
	case SYS_POWER_OFF:
		mode=STM32_BOOT_POWER_OFF;
		break;
	default:
		return NOTIFY_BAD;
	}

	i2c_xfer(to_i2c_client(dev), STM32_WRITE, STM32_REG_CONTROL, 1, mode);

	return NOTIFY_OK;
}

// initialization stuff
static void stm32_gpio_init(struct device *dev, struct stm32 *stm32)
{
	struct gpio_chip *gpio=&stm32->gpiochip;

	// init gpio_chip
	gpio->label=DRIVER_NAME;
	gpio->parent=dev;
	gpio->owner=THIS_MODULE;
	gpio->request=stm32_gpio_request;
	gpio->get_direction=stm32_gpio_get_dir;
	gpio->direction_input=stm32_gpio_dir_input;
	gpio->direction_output=stm32_gpio_dir_output;
	gpio->get=stm32_gpio_get;
	gpio->set=stm32_gpio_set;
	gpio->set_config=stm32_gpio_set_config;
	gpio->can_sleep=TRUE;
	gpio->base=-1;

	// init irq
	gpio->irq.parent_handler=NULL;
	gpio->irq.num_parents=0;
	gpio->irq.parents=NULL;
	gpio->irq.default_type = IRQ_TYPE_NONE;
	gpio->irq.handler = handle_simple_irq;
	gpio->irq.threaded = true;
	//gpio_irq_chip_set_chip(&gpio->irq,&irq_chip);
	gpio->irq.chip = &irq_chip;
	mutex_init(&stm32->irq_lock);
}


// parse the device tree
static int stm32_parse_dt(struct i2c_client *client, struct stm32 *stm32)
{
	struct device *dev=&client->dev;
	int n,err;

	if(of_property_read_u16(dev->of_node, "ngpios", &stm32->gpiochip.ngpio))
		return -ENOENT;
	dev_info(dev, "%s: ngpios=%d\n", __FUNCTION__, stm32->gpiochip.ngpio);

	// if there are gpios, fill their configuration with defaults
	if(stm32->gpiochip.ngpio)
	{
		stm32->gpiopins=devm_kzalloc(dev,
									  stm32->gpiochip.ngpio *
									  sizeof(*stm32->gpiopins),
									  GFP_KERNEL);
		if(!stm32->gpiopins)
			return -ENOMEM;
		for(n=0; n<stm32->gpiochip.ngpio; n++)
		{
			stm32->gpiopins[n].dir=GPIO_LINE_DIRECTION_IN;
			stm32->gpiopins[n].config=STM32_PINCFG_IN_NP;
		}
	}

	// debug irq
	if(of_property_read_u32(dev->of_node, "cpu-irq", &stm32->cpu_irq) < 0)
		stm32->cpu_irq = -1;

	// read pwm
	err=of_property_count_u32_elems(dev->of_node, "pwm-pins");
	stm32->pwmchip.npwm = err<0 ? 0 :err;
	dev_info(dev, "%s: npwm=%d\n", __FUNCTION__, stm32->pwmchip.npwm);

	if(stm32->pwmchip.npwm)
	{
		stm32->pwmpins=devm_kzalloc(dev,
									stm32->pwmchip.npwm *
									sizeof(*stm32->pwmpins),
									GFP_KERNEL);
		if(!stm32->pwmpins)
			return -ENOMEM;
		for(n=0; n<stm32->pwmchip.npwm; n++)
		{
			err=of_property_read_u32_index(dev->of_node, "pwm-pins",
										   n, &stm32->pwmpins[n].no);
			if(err)
				return err;
		}
	}

	if(of_property_read_u32(dev->of_node, "reboot-delay-ms",
							&stm32->reboot_delay) >= 0)
	{
		stm32->reboot_delay /= 10;
		if(stm32->reboot_delay > U8_MAX)
		{
			dev_warn(dev, "%s: reboot delay out of range, using default\n",
					 __FUNCTION__);
			stm32->reboot_delay=-1;
		}
	}
	else
		stm32->reboot_delay=-1;

#if STM32_DEBUG
	dev_info(dev, "%s: %sinterrupt-controller\n",
			 __FUNCTION__,
			 of_property_read_bool(dev->of_node,
								   "interrupt-controller") ? "" : "no ");
#endif

	return 0;
}

// probe the device
static int stm32_probe(struct i2c_client *client,
					   const struct i2c_device_id *id)
{
	struct device *dev=&client->dev;
	struct stm32 *stm32;
	int err=0;
	int n;

	dev_info(dev, "%s\n", __FUNCTION__);

	// allocate memory for private data
	stm32=devm_kzalloc(dev, sizeof(*stm32), GFP_KERNEL);
	if(!stm32)
		return -ENOMEM;

	// read the chip id
	err=i2c_xfer(client, STM32_READ, STM32_REG_ID, 0);
	if(err < 0)
	{
		dev_err(dev, "%s: error (%d) reading chip id\n", __FUNCTION__, err);
		return err;
	}

	for(n=0; n<ARRAY_SIZE(stm32_id) && err!=stm32_id[n]; n++);
	if(n==ARRAY_SIZE(stm32_id))
	{
		dev_err(dev, "%s: invalid chip id (0x%02x)\n", __FUNCTION__, err);
		return -ENOENT;
	}
	stm32->chip_id=(unsigned char)err;
#if STM32_DEBUG
	dev_info(dev, "%s: chip id: 0x%02x\n", __FUNCTION__, err);
#endif

	// read firmware version
	err=i2c_xfer(client, STM32_READ, STM32_REG_VERSION, 0);
	if(err < 0)
	{
		dev_err(dev, "%s: error (%d) reading firmware version\n",
				__FUNCTION__, err);
		return err;
	}
	stm32->fw_ver=(unsigned char)err;
#if STM32_DEBUG
	dev_info(dev,"%s: firmware 0x%02x\n", __FUNCTION__, err);
#endif

	// read seco-code
	err=i2c_xfer(client, STM32_READ, STM32_REG_SECO_CODE, 0);
	if(err < 0)
	{
		dev_err(dev, "%s: error (%d) reading seco-code\n",
				__FUNCTION__, err);
		return err;
	}
	stm32->seco_code=(unsigned char)err;
#if STM32_DEBUG
	dev_info(dev,"%s: seco-code 0x%02x\n", __FUNCTION__, err);
#endif

	// fill private data from the device tree
	err=stm32_parse_dt(client, stm32);
	if(err)
		return err;
	dev_set_drvdata(dev, stm32);

	// add gpio_chip
	if(stm32->gpiochip.ngpio)
	{
		stm32_gpio_init(dev,stm32);
		err=devm_gpiochip_add_data(dev, &stm32->gpiochip, stm32);
		if(err)
			return err;
	}

	// add pwm chip
	if(stm32->pwmchip.npwm)
	{
		stm32->pwmchip.dev=dev;
		stm32->pwmchip.ops=&pwm_ops;
		err=devm_pwmchip_add(dev, &stm32->pwmchip);
		if(err)
			return err;
		for(n=0; n<stm32->pwmchip.npwm; n++)
			stm32->gpiopins[stm32->pwmpins[n].no].dir=-EBUSY;
	}

	// install interrupt handler
	if(client->irq && stm32->gpiochip.ngpio)
	{
#if STM32_DEBUG
		dev_info(dev, "%s: request irq %d\n", __FUNCTION__, client->irq);
#endif
		err=devm_request_threaded_irq(dev, client->irq,
									  NULL, stm32_irq_handler,
									  IRQF_TRIGGER_FALLING | IRQF_ONESHOT,
									  DRIVER_NAME, stm32);
		if(err)
		{
			dev_err(dev, "%s: failed to install irq-handler, err=%d\n",
					__FUNCTION__, err);
			return err;
		}

		if(stm32->cpu_irq >= 0)
		{
			int reg;

#if STM32_DEBUG
			dev_info(dev, "%s: using pin %d for cpu-irq\n",
					 __FUNCTION__, stm32->cpu_irq);
#endif
			stm32->irqmask.mask32 = (1<<stm32->cpu_irq);
			err=i2c_xfer(client, STM32_WRITE, STM32_REG_PIN_CONFIG, 2,
						 stm32->cpu_irq, STM32_PINCFG_IRQ_EDGE_FALLING);
			for(n=0, reg=STM32_REG_INT_ENABLE0;
				!err && n<STM32_REG_INT_COUNT; n++, reg+=2)
				if(stm32->irqmask.mask8[n])
				{
					err=i2c_xfer(client, STM32_WRITE, reg, 1,
								 stm32->irqmask.mask8[n]);
					break;
				}

			if(err)
				dev_warn(dev, "%s: failed to install pin %d as cpu-irq\n",
						 __FUNCTION__, stm32->cpu_irq);
		}
	}

	// reboot
	stm32->bootmode = STM32_BOOT_DEFAULT;
	stm32->reboot.notifier_call = stm32_reboot;
	stm32->reboot.priority = SYS_OFF_PRIO_DEFAULT;
	devm_register_reboot_notifier(dev, &stm32->reboot);
	if(stm32->reboot_delay >= 0)
	{
		err=i2c_xfer(client, STM32_WRITE, STM32_REG_SHUTDOWN_DELAY,
					 1, stm32->reboot_delay);
		if(err)
			dev_warn(dev, "%s: failed to set reboot delay\n", __FUNCTION__);
	}

	// add sysfs entries
	device_create_file(dev, &dev_attr_chip_id);
	device_create_file(dev, &dev_attr_fw_ver);
	device_create_file(dev, &dev_attr_seco_code);
	device_create_file(dev, &dev_attr_boot_mode);

	dev_info(dev, "%s: OK\n", __FUNCTION__);

	return 0;
}

static struct of_device_id seco_stm32_match_table[] = {
	{ .compatible = "seco,stm32",},
};

static struct i2c_driver seco_stm32_i2c_driver = {
	.probe = stm32_probe,
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = seco_stm32_match_table,
	},
};

module_i2c_driver(seco_stm32_i2c_driver);

MODULE_AUTHOR("Dietmar Muscholik <dietmar.muscholik@seco.com>");
MODULE_DESCRIPTION("STM32-based GPIO-extenter and Pulse-Width-Modulator "\
				   "by SECO NE");
MODULE_LICENSE("GPL v2");
