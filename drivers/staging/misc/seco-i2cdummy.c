/*****************************************************************************/
/* SPDX-License-Identifier: (GPL-2.0+ OR MIT)                                */
/*
 * i2c dummy driver
 *
 * Copyright 2024 SECO NE    Dietmar Muscholik <dietmar.muscholik@seco.com>
 *
 *****************************************************************************/

#include <linux/of.h>
#include <linux/i2c.h>
#include <linux/gpio/consumer.h>
#include <linux/delay.h>

static int i2cdummy_send(struct i2c_client *client,
						 u32 *buffer, size_t buff_size)
{
	int err=0;
	u8 buf[2];
	struct i2c_msg msg = {
		.addr = client->addr,
		.flags = 0,
		.len = sizeof(buf),
		.buf = buf,
	};
	size_t count;

	for(count=0; count<buff_size; count+=2)
	{
		buf[0]=(u8)buffer[count];
		buf[1]=(u8)buffer[count+1];
		err=i2c_transfer(client->adapter, &msg, 1);
		if(err<0)
			return err;
	}
	
	return 0;
}


static int i2cdummy_probe(struct i2c_client *client,
						  const struct i2c_device_id *id)
{
	int err=0;
	struct gpio_desc *enable_gpio;
	int seq_count;
	u32 *sequence;

	dev_info(&client->dev, "%s\n", __FUNCTION__);
	enable_gpio=devm_gpiod_get_optional(&client->dev,
										"enable", GPIOD_OUT_LOW);
	if(IS_ERR(enable_gpio))
	{
		err=PTR_ERR(enable_gpio);
		dev_err(&client->dev, "%s: failed to request GPIO: %d\n",
				__FUNCTION__, err);
		return err;
	}

	seq_count=of_property_count_u32_elems(client->dev.of_node, "i2c-sequence");
	if(seq_count <= 0)
	{
		dev_err(&client->dev, "%s: no i2c-sequence found\n",
				__FUNCTION__);
		return seq_count;
	}
	if(seq_count & 1)
	{
		dev_err(&client->dev, "%s: size of i2c-sequence must be even\n",
				__FUNCTION__);
		return -EINVAL;
	}

	sequence=kmalloc((size_t)seq_count * sizeof(*sequence), 0);
	if(!sequence)
		return -ENOMEM;

	err=of_property_read_u32_array(client->dev.of_node, "i2c-sequence",
								  sequence, seq_count);
	if(!err)
	{
		if(enable_gpio)
		{
			gpiod_set_value(enable_gpio, 0);
			usleep_range(10000, 11000);
			gpiod_set_value(enable_gpio, 1);
			usleep_range(1000, 1100);
		}
		err=i2cdummy_send(client, sequence, seq_count);
		if(err)
			dev_err(&client->dev, "%s: failed to send i2c sequence: %d\n",
					__FUNCTION__, err);
	}

	kfree(sequence);
	return err;
}

static int i2cdummy_remove(struct i2c_client *client)
{
	dev_info(&client->dev, "%s\n", __FUNCTION__);
	return 0;
}

static struct of_device_id i2cdummy_match_table[] = {
	{ .compatible = "seco,i2cdummy",},
	{},
};
MODULE_DEVICE_TABLE(of, i2cdummy_match_table);

static struct i2c_driver i2cdummy_driver = {
	.probe = i2cdummy_probe,
	.remove = i2cdummy_remove,
	.driver = {
		.name = "seco-i2cdummy",
		.owner = THIS_MODULE,
		.of_match_table = i2cdummy_match_table,
	},
};
module_i2c_driver(i2cdummy_driver);

MODULE_AUTHOR("Dietmar Muscholik <dietmar.muscholik@seco.com>");
MODULE_DESCRIPTION("i2c dummy driver by SECO-NE");
MODULE_LICENSE("GPL v2");
